import sys, traceback
import os
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QGridLayout, QGroupBox, QFileSystemModel, QTreeView, \
    QGraphicsView, QLabel, QWidget, QPushButton, QTabWidget, QToolButton, QGraphicsScene, QFileDialog, QLineEdit, \
    QCheckBox, QTableWidget, QSpinBox, QTableWidgetItem, QProgressBar, QTableView, QHeaderView, QFormLayout, QSizePolicy, \
    QVBoxLayout, QHBoxLayout, QMessageBox, QComboBox
from PyQt5.QtCore import Qt, QDir, QThread, pyqtSignal, QAbstractTableModel, QRunnable, pyqtSlot, QObject, QThreadPool
from PyQt5.QtGui import QImage, QPixmap, QFont
from SharePoint_Interface import Plum_Interface, BigUpload
from RunInfoExtract import RunFolder, meta_output, zip_folder
from SysInfoCompare import compare_Run_Sysinfo
import pandas as pd
from datetime import datetime
from urllib.parse import quote
from fbs_runtime.application_context.PyQt5 import ApplicationContext

sys.setrecursionlimit(5000)

class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.action = Actions(self)
        self.createMenu()
        self.setFixedSize(600, 400)
        self.setWindowTitle("EGI Seq Run Tracker")
        self.threadpoool = QThreadPool()
        self.createCentralWidget()
        self.show()

    def createMenu(self):
        menuBar = self.menuBar()
        setting = menuBar.addMenu("&Setting")
        setting.addAction("&Setting", self.action.setting)
        verisionInfo = menuBar.addMenu("&Help")
        verisionInfo.addAction("&Help", self.action.helpInfo)

    def createCentralWidget(self):

        layout = self.create_LoginDialog()

        centralWidget = QWidget()
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

    def create_LoginDialog(self):
        self.loggin_form = QWidget()
        form = QFormLayout()

        self.username_le = QLineEdit()
        self.username_le.setFont(QFont("Arial", 18))

        self.user_app_pw_le = QLineEdit()
        self.user_app_pw_le.setEchoMode(QLineEdit.Password)

        form.addRow("Username:", self.username_le)
        form.addRow("App Password:", self.user_app_pw_le)

        self.loggin_button = QToolButton()
        self.loggin_button.setText("Login SharePoint")
        self.loggin_button.setToolTip("Enter User Credentials\nBGI Email and SharePoint App password")
        self.loggin_button.setFixedSize(200, 50)
        self.loggin_button.clicked.connect(self.threadLogin)

        hbox = QHBoxLayout()
        hbox.addStretch()
        hbox.addWidget(self.loggin_button)
        hbox.addStretch()

        self.statustext = QLabel()

        stat_text_hbox = QHBoxLayout()
        stat_text_hbox.addStretch()
        stat_text_hbox.addWidget(self.statustext)
        stat_text_hbox.addStretch()

        vbox = QVBoxLayout()
        vbox.addStretch()
        vbox.addLayout(form)
        vbox.addStretch()
        vbox.addLayout(hbox)
        vbox.addStretch()
        vbox.addLayout(stat_text_hbox)
        return vbox

    def threadLogin(self):
        self.statustext.setText("Attempting to Log in")
        self.login_username = self.username_le.text()
        self.login_pw = self.user_app_pw_le.text()
        worker = ThreadWorker(self.loginclicked)
        worker.signals.result.connect(self.thread_result)

        self.threadpoool.start(worker)

    def loginclicked(self, progress_callback):
        return Plum_Interface(self.login_username, self.login_pw)

    def thread_result(self, inter):
        self.interface = inter
        if self.interface.auth:
            # authorized
            self.statustext.setText("Success")
            self.launchDialog_Login_Success()
        else:
            self.statustext.setText("Failed")
            #not authorized popup
            self.not_authorized()

    def not_authorized(self):
        self.username_le.setText("")
        self.user_app_pw_le.setText("")
        msg = QMessageBox()
        msg.setWindowTitle("Unauthorized Loggin")
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Incorrect Loggin, See Loggin Help at:\nhttps://www.google.com/")
        msg.exec_()

    def launchDialog_Login_Success(self):
        dialog = Dialog_Login_Success(self.interface)
        dialog.thread_sysinfo() ######## now threaded
        dialog.exec_()


class Dialog_Login_Success(QDialog):

    sysinfo = False
    save_dir = None

    sysinfo_paths = None

    input_tab = False
    results_tab = False
    upload_results_tab = False
    sysinfo_confirmed = False

    def __init__(self, interface):
        super(Dialog_Login_Success, self).__init__()
        self.binDir = os.path.expanduser('~')
        self.initPath = os.getcwd()
        self.setup_save_folder()
        os.chdir(self.binDir)
        self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)
        self.setFixedSize(1000, 800)
        self._initialWidget()
        self.interface = interface
        self.threadpool = QThreadPool()
        self.tabWidget = QTabWidget()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())  #
        self.zip_name_le = QLabel()
        self.zip_name_le.setText("_".join(["NULL" for x in range(7)]))
        self.iscid_le = QLineEdit()
        self.iscid_le.setText("NULL")
        self.isclot_le = QLineEdit()
        self.isclot_le.setText("NULL")
        self.ircid_le = QLineEdit()
        self.ircid_le.setText("NULL")
        self.irclot_le = QLineEdit()
        self.irclot_le.setText("NULL")
        self.instid = QLineEdit()
        self.instid.setText("NULL")
        self.dnbid = QLineEdit()
        self.dnbid.setText("NULL")


    def _initialWidget(self):
        self.model = QFileSystemModel()
        self.model.setRootPath(QDir.currentPath())

        self.fileSystem = QTreeView()
        self.fileSystem.setModel(self.model)

        self.progress_bar = QProgressBar()
        self.progress_bar.setValue(50)

        sidebar_groupbox = QGroupBox("Import Sequencing Run")
        vbox = QVBoxLayout()
        sidebar_groupbox.setLayout(vbox)

        self.select_run_folder_btn = QPushButton("Select Run Folder")
        self.select_run_folder_btn.setEnabled(False)
        self.select_run_folder_btn.pressed.connect(self.changeLoadDirectory)
        vbox.addWidget(self.select_run_folder_btn)

        self.user_info_btn = QPushButton("Input Run Info")
        self.user_info_btn.setEnabled(False)
        self.user_info_btn.pressed.connect(self.create_userinputTab)
        vbox.addWidget(self.user_info_btn)

        self.user_result_btn = QPushButton("Select Run Result")
        self.user_result_btn.setEnabled(False)
        self.user_result_btn.pressed.connect(self.addResultsTab)
        vbox.addWidget(self.user_result_btn)

        self.confirm_run_btn = QPushButton("Make Slide and\nUpload to Sharepoint")
        self.confirm_run_btn.setEnabled(False)
        self.confirm_run_btn.pressed.connect(self.confirm_cycle_input)
        vbox.addWidget(self.confirm_run_btn)

        self.rside_vbox =  QVBoxLayout()
        self.rside_vbox.addWidget(self.fileSystem)
        self.rside_vbox.addWidget(self.progress_bar)

        self.layout0 = QHBoxLayout()
        self.layout0.addWidget(sidebar_groupbox)
        self.layout0.addLayout(self.rside_vbox)
        self.setLayout(self.layout0)
        self.setWindowTitle("Ecoli Seq Run Results Upload to Tracker")

    def setup_save_folder(self):
        save_path = os.path.join(self.initPath, 'Save_folder')
        try:
            os.mkdir(save_path)
        except FileExistsError:
            pass
        self.save_dir = save_path

    def thread_sysinfo(self):
        worker = ThreadWorker(self.import_sysinfo_meta)
        worker.signals.error.connect(self.print_thread_errors)
        worker.signals.progress.connect(self.update_pbar_value)
        worker.signals.result.connect(self.set_sysinfo_paths)
        worker.signals.finished.connect(self.sysinfo_complete)

        self.threadpool.start(worker)

    def print_thread_errors(self, error_tup):
        print(error_tup)

    def update_pbar_value(self, pbar_update_tup):
        int_value, str_text = pbar_update_tup
        self.progress_bar.setValue(int_value)
        bar_text = ", ".join([str(int_value)+"%", str_text])
        self.progress_bar.setFormat(bar_text)
        self.progress_bar.setAlignment(Qt.AlignCenter)

    def import_sysinfo_meta(self, progress_callback):
        self.interface.connect_site()
        progress_callback.emit((45, "Connecting to Sharepoint"))
        meta_path = os.path.join(self.initPath, 'MetaSheets')
        try:
            os.mkdir(meta_path)
        except FileExistsError:
            pass
        progress_callback.emit((75, "Importing System Info"))
        return self.interface.import_sysinfo_metadata(meta_path)

    def set_sysinfo_paths(self, path_dict):
        self.sysinfo_paths = path_dict
        print("Results set")

    def sysinfo_complete(self):
        self.sysinfo = True
        self.update_pbar_value((100, "System Info Import Complete"))
        self.select_run_folder_btn.setEnabled(True)
        print("System Info Import Complete")
        print(self.sysinfo_paths)

    def changeLoadDirectory(self):
        directory = QFileDialog.getExistingDirectory(self, "Select Directory")
        if directory:
            self.curr_run_folder = RunFolder(directory, self.save_dir)
            #self.load_lineEdit.setText(directory)
            self.fileSystem.setRootIndex(self.model.index(directory))
            self.thread_runfolder()
        else:
            self.fileSystem.setRootIndex(self.model.index(self.binDir))

    def addTabWidget(self):
        self.rside_vbox.addWidget(self.tabWidget)

    def thread_runfolder(self):
        worker = ThreadWorker(self.init_extract_progress)
        worker.signals.error.connect(self.print_thread_errors)
        worker.signals.finished.connect(self.update_runinfo_to_ui)
        worker.signals.progress.connect(self.update_pbar_value)
        self.threadpool.start(worker)

    def init_extract_progress(self, progress_callback):
        progress_callback.emit((15, "Extracting HTML"))
        self.curr_run_folder.extract_summRep_html()
        progress_callback.emit((30, "Extracting Test Area Info"))
        self.curr_run_folder.extract_testarea_info()
        progress_callback.emit((50, "Find and Extract Run Folders"))
        self.curr_run_folder.find_folders()
        self.curr_run_folder.extract_info_from_dirname()
        #move compare to full run after confirm params
        #progress_callback.emit((65, "Compare to System Info"))
        #self.meta_exper_input = compare_Run_Sysinfo(self.curr_run_folder, self.sysinfo_paths)
        progress_callback.emit((100, "Run Folder Extract Complete"))

    def update_runinfo_to_ui(self):
        #self.curr_run_folder.html_file.print_extract_data
        param_info_dict = self.curr_run_folder.return_update_paraminfo()
        param_vals = param_info_dict.values()
        zip_join = [param if param is not None else "NULL" for param in param_vals]
        default_params = {}
        default_text = ["Enter Date, FMT:  YYYYMMDD",
                        "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"]
        for pkey, ptxt in zip(param_info_dict.keys(), default_text):
            default_params[pkey] = ptxt

        ui_info = {}
        for pkey in param_info_dict.keys():
            txt = default_params[pkey] if param_info_dict[pkey] is None else param_info_dict[pkey]
            ui_info[pkey] = txt

        self.zip_f_name = "_".join(zip_join)
        ui_info['SharePoint Folder Upload Name'] = self.zip_f_name
        self.addFolderInputTab(ui_info)
        self.addTabWidget()

    def addFolderInputTab(self, ui_infodict):
        widget = QWidget()
        layout = QGridLayout()

        line_edits = {}

        self.test_date = QLineEdit()
        self.test_date.setText(ui_infodict['Test Date'])
        self.test_date.editingFinished.connect(self.update_date_le)
        line_edits['Test Date'] = self.test_date

        input_labels = ["ISC id", "ISC Lot", "IRC id", "IRC Lot", "Instrument id", "DNB id"]


        self.iscid_le.setText(ui_infodict["ISC id"])
        self.iscid_le.editingFinished.connect(self.update_iscid_le)
        line_edits["ISC id"] = self.iscid_le


        self.isclot_le .setText(ui_infodict["ISC Lot"])
        self.isclot_le.editingFinished.connect(self.update_isclot_le)
        line_edits["ISC Lot"] = self.isclot_le


        self.ircid_le.setText(ui_infodict[ "IRC id"])
        self.ircid_le.editingFinished.connect(self.update_ircid_le)
        line_edits[ "IRC id"] = self.ircid_le


        self.irclot_le.setText(ui_infodict["IRC Lot"])
        self.irclot_le.editingFinished.connect(self.update_irclot_le)
        line_edits["IRC Lot"] = self.irclot_le


        self.instid.setText(ui_infodict["Instrument id"])
        self.instid.editingFinished.connect(self.update_instid_le)
        line_edits["Instrument id"] = self.instid


        self.dnbid.setText(ui_infodict["DNB id"])
        self.dnbid.editingFinished.connect(self.update_dnbid_le)
        line_edits["DNB id"] = self.dnbid



        row = 0
        for r_idx, label_idx in enumerate(line_edits.keys()):
            layout.addWidget(QLabel(label_idx), r_idx, 0)
            layout.addWidget(line_edits[label_idx], r_idx, 1)
            row = r_idx

        self.zip_name_le.setText(self.zip_f_name)
        sp_label = QLabel()
        sp_label.setText('SharePoint Folder Upload Name')

        layout.addWidget(sp_label, row+1, 0, 1, 2)
        layout.addWidget(self.zip_name_le, row+2, 0 ,1, 2)


        self.confirm_sysinfo_btn = QPushButton()
        self.confirm_sysinfo_btn.setText("Confirm Run System Information")
        self.confirm_sysinfo_btn.pressed.connect(self.thread_compare)
        layout.addWidget(self.confirm_sysinfo_btn, row+3, 0, 1, 2)

        widget.setLayout(layout)
        self.tabWidget.addTab(widget, 'Run System Information')

    def thread_compare(self):
        worker = ThreadWorker(self.confirm_system_info)
        worker.signals.error.connect(self.print_thread_errors)
        worker.signals.finished.connect(self.addSysInfoTab)
        worker.signals.progress.connect(self.update_pbar_value)
        self.threadpool.start(worker)

    def confirm_system_info(self, progress_callback):
        if self.sysinfo_confirmed:
            self.tabWidget.removeTab(1)

        print("confirm system info from ui")
        progress_callback.emit((15, "Confirming Run Parameters"))
        param_keys = ["Test Date", "ISC id", "ISC Lot", "IRC id", "IRC Lot", "Instrument id", "DNB id"]
        param_order = [self.test_date, self.iscid_le, self.isclot_le, self.ircid_le,
                       self.irclot_le, self.instid, self.dnbid]
        param_dict = {}
        for pkey, p_var in zip(param_keys, param_order):
            param_dict[pkey] = p_var.text()
        self.zip_name_le.setText("_".join(param_dict.values()))
        self.curr_run_folder.return_update_paraminfo(update_dict=param_dict)
        progress_callback.emit((65, "Compare to System Info"))
        self.meta_exper_input = compare_Run_Sysinfo(self.curr_run_folder, self.sysinfo_paths)
        self.update_runinfo_to_ui()

        progress_callback.emit((100, "Complete"))

        self.user_info_btn.setEnabled(True)
        self.sysinfo_confirmed = True

    def addSysInfoTab(self):
        old_input = """
        exper_input_dict = {}

        dnb_id = self.curr_run_folder.dnb_id
        # add lookup to dnb sample id, retrieve cols 1, 2a, 2b, 3

        isc_id = self.curr_run_folder.isc_id
        # add lookup to isc id, retrieve mfg date, lot #, SN, pass/fail determ

        irc_id = self.curr_run_folder.irc_id
        # add lookup to irc id, retrieve mfg date, lot #, col 4, SQC

        inst_id = self.curr_run_folder.inst_id
        # add lookup to inst id, retrieve owner, instrument status, DNBSEQE version

        exper_input_dict['DNB ID'] = dnb_id
        exper_input_dict['ISC ID'] = isc_id
        exper_input_dict['IRC ID'] = irc_id
        exper_input_dict['Instrument ID'] = inst_id

        run_df = pd.DataFrame.from_dict(exper_input_dict, orient='index',
                                        columns=['Run %s' % self.curr_run_folder.date])"""
        transpose_row = self.meta_exper_input.show_row
        self.display_rundf = transpose_row
        layout = QGridLayout()
        row = 0
        cur_isclot, cur_irclot = self.curr_run_folder.isc_lot, self.curr_run_folder.irc_lot
        if cur_isclot == 'NULL':
            row_isclot = transpose_row[1]["ISC Lot #"].values[0]
            if not pd.isna(row_isclot):
                update_isclot = row_isclot.replace("lot", "")
                update_isclot = update_isclot.replace("Lot", "")
                self.isclot_le.setText(update_isclot)
        if cur_irclot == 'NULL':
            row_irclot = transpose_row[2]['IRC Lot #'].values[0]
            if not pd.isna(row_irclot):
                self.irclot_le.setText(row_irclot)
        param_keys = ["Test Date", "ISC id", "ISC Lot", "IRC id", "IRC Lot", "Instrument id", "DNB id"]
        param_order = [self.test_date, self.iscid_le, self.isclot_le, self.ircid_le,
                       self.irclot_le, self.instid, self.dnbid]
        param_dict = {}
        for pkey, p_var in zip(param_keys, param_order):
            param_dict[pkey] = p_var.text()
        self.zip_name_le.setText("_".join(param_dict.values()))
        self.curr_run_folder.return_update_paraminfo(update_dict=param_dict)

        for r_idx, df in enumerate(transpose_row):
            view = QTableView()
            model = pandasModel(df)
            view.setModel(model)
            layout.addWidget(view, r_idx, 0)
            row = r_idx
        # header = view.horizontalHeader()
        # header.setSectionResizeMode(0, QHeaderView.ResizeToContents)
        close_results_button = QPushButton("Close Results Tab, Reset")
        close_results_button.clicked.connect(self.closeResultsTab)  #check this button, make sure it flows user experience
        layout.addWidget(close_results_button, row+1, 0)
        widget = QWidget()
        widget.setLayout(layout)
        self.tabWidget.addTab(widget, "Run Folder System Info Results")
        self.tabWidget.setCurrentIndex(0)

    def create_userinputTab(self):
        if not self.input_tab:
            self.confirm_sysinfo_btn.setEnabled(False)
            widget = QWidget()
            layout = QGridLayout()

            line_edits = {}

            self.test_name = QLineEdit()
            self.test_name.setText("")
            line_edits['Test Name'] = self.test_name

            self.se_cycle_number = QLineEdit()
            self.se_cycle_number.setText("")
            line_edits['# SE Cycles'] = self.se_cycle_number

            self.pe_cycle_number = QLineEdit()
            self.pe_cycle_number.setText("")
            line_edits['# PE Cycles'] = self.pe_cycle_number

            self.operator = QLineEdit()
            self.operator.setText("")
            line_edits['Operator'] = self.operator

            self.humidity = QLineEdit()
            self.humidity.setText("")
            line_edits['Lab Humidity (%)'] = self.humidity

            self.labtemp = QLineEdit()
            self.labtemp.setText("")
            line_edits['Lab Temperature (deg. C)'] = self.labtemp

            self.seqPrep_sop_ver = QLineEdit()
            self.seqPrep_sop_ver.setText("")
            line_edits['Seq Prep. SOP'] = self.seqPrep_sop_ver

            self.script_ver = QLineEdit()
            self.script_ver.setText("")
            line_edits['Script Version'] = self.script_ver

            self.bubbl_obs  = QLineEdit()
            self.bubbl_obs.setText("")
            line_edits['Bubble Observation'] = self.bubbl_obs

            self.leak_obs = QLineEdit()
            self.leak_obs.setText("")
            line_edits['Leak Observation'] = self.leak_obs

            self.notes_addnl = QLineEdit()
            self.notes_addnl.setText("")
            line_edits['Additional Notes'] = self.notes_addnl

            for r_idx, label_idx in enumerate(line_edits.keys()):

                layout.addWidget(QLabel(label_idx), r_idx, 0)
                layout.addWidget(line_edits[label_idx], r_idx, 1)

            widget.setLayout(layout)
            self.tabWidget.addTab(widget, 'Run Info User Input')
            self.input_tab = True
            self.user_result_btn.setEnabled(True)

    def update_date_le(self):
        text_change = self.test_date.text()
        if len(text_change) != 8:  #not enough digit in date
            self.test_date.setText("Enter Date, format:  YYYYMMDD")
            return
        try:
            time = datetime.strptime(text_change, '%Y%m%d')
        except Exception as e:  #cannot format to date
            self.test_date.setText("Enter Date, format:  YYYYMMDD")
            return
        self.curr_run_folder.date = text_change
        self.update_zip_name_UI({'Test Date':text_change})

    def update_iscid_le(self):
        self.update_zip_name_UI({'ISC id': self.iscid_le.text()})

    def update_isclot_le(self):
        self.update_zip_name_UI({'ISC Lot': self.isclot_le.text()})

    def update_ircid_le(self):
        self.update_zip_name_UI({'IRC id': self.ircid_le.text()})

    def update_irclot_le(self):
        self.update_zip_name_UI({'IRC Lot': self.irclot_le.text()})

    def update_instid_le(self):
        self.update_zip_name_UI({'Instrument id': self.instid.text()})

    def update_dnbid_le(self):
        self.update_zip_name_UI({'DNB id': self.dnbid.text()})

    def update_zip_name_UI(self, update_dict):
        param_keys = ["Test Date", "ISC id", "ISC Lot", "IRC id", "IRC Lot", "Instrument id", "DNB id"]
        cur_params = self.zip_name_le.text().split("_")
        # "20210101_123456789_12345_L123456789_12345_G00_DNB1234"
        param_dict = {}
        for pkey, cur_p in zip(param_keys, cur_params):
            param_dict[pkey] = cur_p

        for update_key in update_dict.keys():
            param_dict[update_key] = update_dict[update_key]

        new_zipname = "_".join(param_dict.values())
        self.zip_name_le.setText(new_zipname)

    def addResultsTab(self):

        if not self.results_tab:
            failure_root_modes = [
                'None - Pass',
                'Instr - ME',
                'Instr - EE',
                'Instr - SW',
                'Instr - Suspect',
                'Cart - ISC (surface)',
                'Cart - ISC (white lines)',
                'Cart - IRC',
                'Biochem - reagent',
                'Biochem - recipe',
                'Biochem - operator',
                'Sample',
                'Biochem - susspect',
                'Else/Unknown',
                'Underloading'
            ]
            widget = QWidget()
            layout = QGridLayout()
            results_tab_dict = {}

            self.se_result = QComboBox()
            self.se_result.addItems(['Pass', 'Fail', 'N/A'])
            results_tab_dict['SE Result'] = self.se_result

            self.pe_result = QComboBox()
            self.pe_result.addItems(['Pass', 'Fail', 'N/A'])
            results_tab_dict['PE Result'] = self.pe_result

            self.fail_cause_prim = QComboBox()
            self.fail_cause_prim.addItems(failure_root_modes)
            self.fail_cause_prim.itemText(0)
            results_tab_dict['Primary Failure Cause'] = self.fail_cause_prim

            self.fail_cause_sec = QComboBox()
            self.fail_cause_sec.addItems(failure_root_modes)
            self.fail_cause_sec.itemText(0)
            results_tab_dict['Secondary Failure Cause'] = self.fail_cause_sec

            self.upload_dir = QLineEdit()
            self.upload_dir.setText('Shared Documents/DNBSEQ E tracker/Individual runs')
            results_tab_dict['Sharepoint Upload Folder'] = self.upload_dir

            for r_idx, label_idx in enumerate(results_tab_dict.keys()):

                layout.addWidget(QLabel(label_idx), r_idx, 0)
                layout.addWidget(results_tab_dict[label_idx], r_idx, 1)

            widget.setLayout(layout)
            self.tabWidget.addTab(widget, 'Run Result User Input')
            self.results_tab = True
            self.confirm_run_btn.setEnabled(True)

    def closeResultsTab(self):
        self.tabWidget.clear()  # remove all tabs
        self.display_rundf = None
        self.meta_exper_input = None
        self.user_result_btn.setEnabled(False)
        self.user_info_btn.setEnabled(False)
        self.confirm_run_btn.setEnabled(False)
        self.fileSystem.setRootIndex(self.model.index(self.binDir))
        self.input_tab = False
        self.results_tab = False
        self.select_run_folder_btn.setEnabled(True)
        self.upload_results_tab = False

    def thread_run(self):
        self.select_run_folder_btn.setEnabled(False)
        self.user_info_btn.setEnabled(False)
        self.user_result_btn.setEnabled(False)
        self.confirm_run_btn.setEnabled(False)
        worker = ThreadWorker(self.confirm_run)
        worker.signals.error.connect(self.print_thread_errors)
        worker.signals.progress.connect(self.update_pbar_value)
        worker.signals.result.connect(self.locked_popup)
        worker.signals.finished.connect(self.run_complete)

        self.threadpool.start(worker)

    def confirm_run(self, progress_callback):
        run_name = self.zip_name_le.text()
        self.curr_run_folder.finalize_params(run_name)
        zip_f_name = run_name + "_UI"
        zip_save_dir = self.save_dir+"\\%s"%zip_f_name

        self.make_uiInput_df_row()
        self.make_img_scatter_dfrow(zip_f_name)
        progress_callback.emit((5, "Create New Tracker Row"))
        self.make_meta_row()
        progress_callback.emit((15, "Output New Tracker Master"))
        fail = self.output_metaTracker()
        progress_callback.emit((45, "Make Slide"))
        self.make_slide_lbardict()
        self.curr_run_folder.make_a_slide("_".join([zip_f_name, self.slide_lbar_dict['Name']]),
                                          self.slide_lbar_dict)
        progress_callback.emit((50, "Zipping Folder"))
        ret = self.curr_run_folder.zip_runfolder(zip_save_dir, zip_f_name)
        progress_callback.emit((75, "Uploading to Sharepoint, Please wait..."))
        if ret[0]:
            bigups = """
            upload = BigUpload(self.interface, config)
            b = upload.do_upload(zip_save_dir, zip_name)
            print(b)"""
            print("Complete")
            upload_dir = str(self.upload_dir.text())
            print(upload_dir)
            doc_lib = upload_dir.split("/")[0]
            join_lst = upload_dir.split("/")[1:]
            join_filter = [x for x in join_lst if x != ""]
            doc_path = "/".join(join_filter)
            config = {}
            config['sp_base_path'] = 'https://bgitech.sharepoint.com'
            config['sp_site_name'] = 'ProductConceptInstrumentBiochemIntegration'
            config['sp_doc_library'] = doc_lib
            config['sp_path'] = doc_path
            for zip_segment in ret[1]:
                print("Check in %s"%zip_segment[1])
                self.interface.checkin_file(zip_segment[0],
                                        upload_dir, zip_segment[1])
        else:
            print("Fail")
        #progress_callback.emit((90, "Reset Save Folder"))
        #self.reset_save_folder()
        progress_callback.emit((100, "Run Complete"))
        if fail:
            return True
        else:
            return False

    def confirm_cycle_input(self):

        se_cycles_txt = self.se_cycle_number.text()
        pe_cycles_text = self.pe_cycle_number.text()

        try:
            se_cycles = int(se_cycles_txt)
            pe_cycles = int(pe_cycles_text)
        except ValueError:
            print("Cannot parse txt input to int")
            self.se_cycle_number.setText("")
            self.pe_cycle_number.setText("")
            msg = QMessageBox()
            msg.setWindowTitle("Incorrect Value for SE or PE Cycle Number")
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Incorrect value for SE or PE Cycle Number.\nPlease use integer values.")
            msg.exec_()
            return
            #emit invalid cyclenumbers error
        if self.curr_run_folder.html_file is not None:
            cycle_number = int(self.curr_run_folder.html_file.summaryTable['CycleNumber'])
        else:
            cycle_number = 0
        if se_cycles + pe_cycles > cycle_number:
            print("listed more cycles than present in html")
            self.se_cycle_number.setText("")
            self.pe_cycle_number.setText("")
            msg = QMessageBox()
            msg.setWindowTitle("Incorrect Values for SE and PE Cycle Number")
            msg.setIcon(QMessageBox.Critical)
            msg.setText("""
            Incorrect value for SE or PE Cycle Number.\n
            Run folder html file cycle number:\t%s\n
            User entered SE, PE cycle numbers:\t%s, %s\n
            %s > %s
            """%(cycle_number, se_cycles, pe_cycles, se_cycles+pe_cycles, cycle_number))
            msg.exec_()
            return
        else:
            self.thread_run()

    def make_img_scatter_dfrow(self, zipfname):
        upload_folder_split = self.upload_dir.text().split("/")
        folder = []
        for split in upload_folder_split:
            if "ProductConceptInstrumentBiochemIntegration" in split:
                continue
            elif "Shared Documents" in split:
                continue
            else:
                folder.append(split)
        share_folder = "/".join(folder)
        parent_all = []
        if self.curr_run_folder.scatter_dir is not None:
            scatter_paths, parent_scatter = self.curr_run_folder.scatter_dir.first_last_sharepointpath()
            parent_all.extend([parent_scatter, parent_scatter])
        else:
            scatter_paths = [None, None]
            parent_all.extend([None, None])
        if self.curr_run_folder.rawimg_dir is not None:
            img_paths, parent_scale = self.curr_run_folder.rawimg_dir.first_last_sharepoint_link()
            parent_all.extend([parent_scale, parent_scale])
        else:
            img_paths = [None, None]
            parent_all.extend([None, None])
        img_paths.extend(scatter_paths)
        df_paths = ["" if None else x for x in img_paths]
        col_labels = ['Cycle 1 Image', 'Last Cycle Image', 'Cycle 1 Scatter', 'Last Cycle Scatter']
        df_rowdict = {}

        sp_link_start = ["https://bgitech.sharepoint.com/sites/ProductConceptInstrumentBiochemIntegration/",
                        "Shared%20Documents/Forms/AllItems.aspx?sortField=Modified&isAscending=false&id="]
        sp_link_main_path = ["%2Fsites%2FProductConceptInstrumentBiochemIntegration%2FShared%20Documents%2FDNBSEQ%20E%20tracker%",
                        "2FIndividual%20runs%2F"]
        sp_link_start.extend(sp_link_main_path)

        sp_folder = zipfname.replace(" ", "_")
        sp_folder = sp_folder.replace(".", "%2E")
        sp_folder_spl = sp_folder.split("_")

        sp_ending = ['&parent=']
        sp_ending.extend(sp_link_main_path)
        sp_ending.extend(sp_folder_spl)
        for col_label, path, parent_f in zip(col_labels, df_paths, parent_all):
            reset = """
            split = path.split("\\")[1:]
            dirname = self.curr_run_folder.dirname.split("/")[-1]
            share_split = share_folder.split("/")
            home_site = self.interface.home_site[:-1]
            join_list = [home_site, 'Shared Documents']
            join_list.extend(share_split)
            join_list.append(zipfname)
            join_list.append(dirname)
            join_list.extend(split)
            join_path = "/".join(join_list)
            #print(split, dirname, share_split)
            #print(join_list)
            out_path = join_path.replace(" ", "%20")"""
            if parent_f is None:
                df_rowdict[col_label] = ""
            else:
                sp_link_start = "".join(sp_link_start)  #join start link
                sp_folder_name = "%5F".join(sp_folder_spl)  #join underscores of folder name
                sp_local_f = sp_folder_name+"%2F"+path    #combine folder name and img path with /
                ending_label = "".join(sp_ending) + "%2F"+ parent_f  #combine ending with parent folder name
                out_path = "".join([sp_link_start, sp_local_f, ending_label])   #combine start, localf, parent
                df_rowdict[col_label] = out_path

        df = pd.DataFrame(df_rowdict, index=[0])
        self.img_scatter_dfrow = df

    def make_meta_row(self):
        se_first_cycle = 0
        pe_first_cycle = int(self.se_cycle_number.text()) + 1
        if int(self.pe_cycle_number.text()) == 0:
            pe_first_cycle = None
        dfs = [self.uiInput_df, self.meta_exper_input.return_row,
               self.img_scatter_dfrow, self.curr_run_folder.make_testarea_dfrow(),
               self.curr_run_folder.make_html_dfrow(se_first_cycle, pe_first_cycle)]
        col_list = [list(x.columns.values) for x in dfs]
        prepend_values = ['User Input ', 'Experiment Input ', 'Image/Scatter ', 'Testing Area ', 'HTML Input ']
        df_list_out = []
        for prep_col_str, df in zip(prepend_values, dfs):
            cur_cols = col_list.pop(0)
            other_cols = []
            for i in range(len(col_list)):
                other_cols.extend(col_list[i])
            dup_cols = set(cur_cols) & set(other_cols)
            rename_cols = {}
            for dup in dup_cols:
                rename_cols[dup] = prep_col_str + dup
            renamed_df = df.rename(rename_cols, axis = 1)
            df_list_out.append(renamed_df)
            col_list.append(cur_cols)
        # remove duplicate column names, prepend string from category

        total_df_row = pd.concat(df_list_out, axis=1)
        #print(total_df_row.loc[:, total_df_row.columns.str.contains("Mapped")])
        self.meta_row = total_df_row

    def make_uiInput_df_row(self):

        df_row = {}
        df_row['Test Date'] = str(self.test_date.text())
        df_row['Test Name'] = str(self.test_name.text())
        df_row['SE Cycle Length'] = str(self.se_cycle_number.text())
        df_row['PE Cycle Length'] = str(self.pe_cycle_number.text())
        df_row['Operator'] = str(self.operator.text())
        df_row['Lab Humidity (%)'] = str(self.humidity.text())
        df_row['Lab Temperature (deg. C)'] = str(self.labtemp.text())
        df_row['Seq Prep. SOP version'] = str(self.seqPrep_sop_ver.text())
        df_row['Script Version'] = str(self.script_ver.text())
        df_row['Bubble Observation'] = str(self.bubbl_obs.text())
        df_row['Any Other Notes'] = str(self.notes_addnl.text())
        df_row['SE Result'] = str(self.se_result.currentText())
        df_row['PE Result'] = str(self.pe_result.currentText())
        df_row['If Failed\nPrimary failure cause'] = str(self.fail_cause_prim.currentText())
        df_row['If Failed\nSecondary failure cause'] = str(self.fail_cause_sec.currentText())
        df_row['Sharepoint Directory'] = str(self.upload_dir.text())
        df = pd.DataFrame(df_row, index=[0])
        self.uiInput_df = df

    def make_slide_lbardict(self):
        folder_input = self.meta_exper_input.return_row

        make_a_slide_dict = {}
        make_a_slide_dict['Date'] = self.curr_run_folder.date
        make_a_slide_dict['Name'] = self.uiInput_df['Test Name'].values[0]
        make_a_slide_dict['Instr'] = folder_input['Instrument Type'].values[0]
        make_a_slide_dict['Instr ID'] = folder_input['Instrument ID'].values[0]
        make_a_slide_dict['DNBSEQ E Ver'] = folder_input['DNBSEQ E version'].values[0]
        make_a_slide_dict['SW Ecoli Seq'] = folder_input['SW_Ecoli Sequence'].values[0]
        make_a_slide_dict['Seq Prep. SOP Ver.'] = self.uiInput_df['Seq Prep. SOP version'].values[0]
        make_a_slide_dict['IRC Lot'] = folder_input['IRC Lot #'].values[0]
        make_a_slide_dict['ISC Lot'] = folder_input['ISC Lot #'].values[0]
        make_a_slide_dict['DNB ID'] = self.curr_run_folder.dnb_id
        make_a_slide_dict['SE Result'] = self.uiInput_df['SE Result'].values[0]
        make_a_slide_dict['PE Result'] = self.uiInput_df['PE Result'].values[0]
        self.slide_lbar_dict = make_a_slide_dict

    def output_metaTracker(self):

        init_ver_dictrow = {}
        columns = ['Upload Date', 'Upload User', 'Index Added', 'Notes']
        df = pd.read_excel(self.sysinfo_paths['DNBSEQ_Tracker_Master.xlsx'][0], sheet_name='Master Tracker', index_col=[0], skiprows=[0])
        cur_idx = int(df.index.values[-1])
        data = [datetime.today().strftime('%Y%m%d'),  str(self.interface.username), cur_idx+1, '']
        for col, data in zip(columns, data):
            init_ver_dictrow[col] = data

        init_ver_df = pd.DataFrame(init_ver_dictrow, index=[0])
        output = meta_output(self.meta_row, self.sysinfo_paths['DNBSEQ_Tracker_Master.xlsx'][0], init_ver_df)
        ret = self.interface.checkin_file(self.sysinfo_paths['DNBSEQ_Tracker_Master.xlsx'][0],
                                          self.interface.tracker_mast_site, 'DNBSEQ_Tracker_Master.xlsx')
        retm = self.interface.checkin_file(self.sysinfo_paths['DNBSEQ_Tracker_Master.xlsx'][0],
                                          self.interface.tracker_mirror_site, 'DNBSEQ_Tracker_Master.xlsx')
        if ret is True:
            return True
        elif '423' in ret:
            return False
        else:
            return True

    def run_complete(self):
        #keep buttons blocked, add new tab
        #self.select_run_folder_btn.setEnabled(True)
        #self.user_info_btn.setEnabled(True)
        #self.user_result_btn.setEnabled(True)
        #self.confirm_run_btn.setEnabled(True)
        if not self.upload_results_tab:
            widget = QWidget()
            layout = QGridLayout()

            line_dict = {}
            line_dict['Upload Folder Name'] = QLabel(self.zip_name_le.text())
            self.up_status_label = QLabel()
            self.up_status_label.setText("Failure" if self.upload_fail else "Success")
            line_dict['Tracker Upload Status'] = self.up_status_label

            self.retry_upload_btn = QPushButton()
            self.retry_upload_btn.setText("Retry Upload")
            self.retry_upload_btn.pressed.connect(self.thread_retry)
            if self.upload_fail:
                self.retry_upload_btn.setEnabled(True)
            else:
                self.retry_upload_btn.setEnabled(False)
            #line_dict['Retry Upload'] = retry_upload_btn

            self.cleanup_btn = QPushButton()
            self.cleanup_btn.setText("Reset and Choose New Run")
            self.cleanup_btn.pressed.connect(self.reset_save_folder)
            #line_dict['Reset'] = self.cleanup_btn
            for r_idx, label_idx in enumerate(line_dict.keys()):
                layout.addWidget(QLabel(label_idx), r_idx, 0, 1, 1, Qt.AlignCenter)
                layout.addWidget(line_dict[label_idx], r_idx, 1, 1, 2, Qt.AlignLeft)
            layout.addWidget(self.retry_upload_btn, 2, 0)
            layout.addWidget(self.cleanup_btn, 2, 1)

            widget.setLayout(layout)
            self.tabWidget.addTab(widget, 'Upload Results')
            self.upload_results_tab = True
            self.tabWidget.setCurrentIndex(3)
        else:
            self.up_status_label.setText("Failure" if self.upload_fail else "Success")
            if self.upload_fail:
                self.retry_upload_btn.setEnabled(True)
            else:
                self.retry_upload_btn.setEnabled(False)
            self.cleanup_btn.setEnabled(True)

    def retry_tracker_upload(self, progress_callback):
        progress_callback.emit((75, "Retry Tracker Upload, Please wait..."))
        ret = self.interface.checkin_file(self.sysinfo_paths['DNBSEQ_Tracker_Master.xlsx'][0],
                                          self.interface.tracker_mast_site, 'DNBSEQ_Tracker_Master.xlsx')
        retm = self.interface.checkin_file(self.sysinfo_paths['DNBSEQ_Tracker_Master.xlsx'][0],
                                           self.interface.tracker_mirror_site, 'DNBSEQ_Tracker_Master.xlsx')
        progress_callback.emit((100, "Complete"))
        if ret is True:
            return True
        elif '423' in ret:
            return False
        else:
            return True

    def thread_retry(self):
        self.cleanup_btn.setEnabled(False)
        self.retry_upload_btn.setEnabled(False)
        worker = ThreadWorker(self.retry_tracker_upload)
        worker.signals.error.connect(self.print_thread_errors)
        worker.signals.progress.connect(self.update_pbar_value)
        worker.signals.result.connect(self.locked_popup)
        worker.signals.finished.connect(self.run_complete)

        self.threadpool.start(worker)

    def reset_save_folder(self):
        files = os.listdir(self.save_dir)
        for f in files:
            path_f = os.path.join(self.save_dir, f)
            os.remove(path_f)
        print(os.listdir(self.save_dir))
        self.closeResultsTab()

    def locked_popup(self, return_val):
        if return_val:
            self.upload_fail = False
            return
        else:
            self.upload_fail = True
            msg = QMessageBox()
            msg.setWindowTitle("Sharepoint DNBSEQ_Tracker_Master Locked")
            msg.setIcon(QMessageBox.Critical)
            msg.setText("""
            "A user has "Locked" the sharepoint file:\n"DNBSEQ_Tracker_Master.xlsx" for editing.\n
            The file cannot be updated until they finish editing and close the file.\n
            Please try again later, or ask all users to close the file.  
            """)
            msg.exec_()
            return



class Actions:
    def __init__(self, window):
        self.window = window

    def helpInfo(self):
        self.window.help_info = QLabel()
        info = "version: V1.55\n"
        self.window.help_info.setText(info)
        self.window.help_info.show()

    def setting(self):
        self.window.statusBar().showMessage("Setting default.")

class ThreadWorker(QRunnable):
    """
    Worker thread
    """

    def __init__(self, fn, *args, **kwargs):
        super(ThreadWorker, self).__init__()
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        """
        Code Goes Here
        """
        try:
            result = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            self.signals.result.emit(result)
        finally:
            self.signals.finished.emit()


class WorkerSignals(QObject):
    '''
      Defines the signals available from a running worker thread.

      Supported signals are:

      finished
          No data

      error
          `tuple` (exctype, value, traceback.format_exc() )

      progress
            int % progress

      result
          `object` data returned from processing, anything

      '''
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    progress = pyqtSignal(tuple)
    result = pyqtSignal(object)

class pandasModel(QAbstractTableModel):

    def __init__(self, data):
        QAbstractTableModel.__init__(self)
        self._data = data

    def rowCount(self, parent=None):
        return self._data.shape[0]

    def columnCount(self, parnet=None):
        return self._data.shape[1]

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:
                return str(self._data.iloc[index.row(), index.column()])
        return None

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._data.columns[col]
        return None


if __name__ == '__main__':
    #appctxt = ApplicationContext()
    app = QApplication([])

    window = MainWindow()
    window.show()

    app.exec_()
    #exit_code = appctxt.app.exec_()
    #sys.exit(exit_code)
