from shareplum import Site
from shareplum import Office365
from shareplum.site import Version
import os
import requests

class Plum_Interface:

    auth_site = 'https://bgitech.sharepoint.com'
    home_site = 'https://bgitech.sharepoint.com/sites/ProductConceptInstrumentBiochemIntegration/'
    home_folder_site = 'Shared Documents/DNBSEQ E tracker/'
    sys_info_folder_site = home_folder_site + "Sys Info/"
    sysinfo_irc_site = sys_info_folder_site + "IRC/"
    sysinfo_isc_site = sys_info_folder_site + "ISC/"
    sysinfo_meta_site = sys_info_folder_site+"Meta System Info/"
    sysinfo_inst_site = sys_info_folder_site+"Instrument/"
    sysinfo_dnb_site = sys_info_folder_site
    iruns_site = home_folder_site +"Individual_runs/testFolder"
    tracker_mast_site = home_folder_site + 'Output tracker file/'
    tracker_mirror_site = tracker_mast_site + 'Expired/Hidden_NoEdit/'


    link_to_instructions = "_____FILL IN LINK HERE_____"

    auth_cookie = None

    site = None
    tracker_folder = None

    def __init__(self, user_name, passw):
        self.username = user_name
        self.password = passw
        self.auth = self.try_authorize()


    def try_authorize(self):
        try:
            self.auth_cookie = Office365(self.auth_site, username=self.username, password=self.password).GetCookies()
            return True
        except Exception as e:
            print(e)
            if 'AADSTS50126' in str(e):
                print("Incorrect Loggin Credentials.  Please Verify username and app password:\n%s"%self.link_to_instructions)
            return False

    def connect_site(self):
        if self.auth_cookie is not None:
            try:
                self.site = Site(self.home_site, version=Version.v365, authcookie=self.auth_cookie)
                self.session = requests.Session()
                self.session.cookies = self.auth_cookie
                self.session.headers.update({'user-agent': 'python_bite/v1'})
                self.session.headers.update({'accept': 'application/json;odata=verbose'})
                self.init_session()
                self.tracker_folder = self.site.Folder(self.home_folder_site)
                print(self.tracker_folder.files)
                print(self.tracker_folder.folders)
            except Exception as e:
                print(e)

    def init_session(self):
        # dirty workaround.... I'm getting the X-RequestDigest from the first failed call
        self.session.headers.update({'X-RequestDigest': 'FormDigestValue'})
        response = self.session.post(
            url=self.home_site + "/_api/web/GetFolderByServerRelativeUrl('" + 'Shared Documents' + "')/Files/add(url='a.txt',overwrite=true)",
            data="")
        self.session.headers.update({'X-RequestDigest': response.headers['X-RequestDigest']})

    def import_sysinfo_metadata(self, save_path):
        #self.sys_info_folder = self.site.Folder(self.sys_info_folder_site)

        self.sys_meta_folder = self.site.Folder(self.sysinfo_meta_site)
        print(self.sys_meta_folder.files)
        return_paths = {}
        for file in self.sys_meta_folder.files:
            new_file_path = self.getfile(file['Name'], self.sys_meta_folder, save_path)
            return_paths[file['Name']] = [new_file_path, file['TimeLastModified']]

        dnb_save_path = self.import_dnb_log(save_path)
        inst_save_path = self.import_inst_log(save_path)
        meta_tracker_savep = self.import_tracker_master(save_path)
        for path in [dnb_save_path, inst_save_path, meta_tracker_savep]:
            name = path[0].split("\\")[-1]
            return_paths[name] = list(path)
        return return_paths

    def import_dnb_log(self, save_path):
        #print(self.sysinfo_dnb_site)
        folder = self.site.Folder((self.sysinfo_dnb_site))
        found = None
        for file in folder.files:
            if "DNB sample" in file['Name']:
                found = file
        if found is not None:
            dnb_file_path = self.getfile(found['Name'], folder, save_path)
            return dnb_file_path, found['TimeLastModified']
        else:
            return None

    def import_inst_log(self, save_path):
        # add lookup for old logs
        # and change E5 or E25
        #print(self.sysinfo_inst_site)
        folder = self.site.Folder((self.sysinfo_inst_site))
        found = None
        for file in folder.files:
            if "DNBSEQ" in file['Name']:
                found = file
        if found is not None:
            inst_fpath = self.getfile(found['Name'], folder, save_path)
            return inst_fpath, found['TimeLastModified']
        else:
            return None

    def import_tracker_master(self, save_path):
        folder = self.site.Folder((self.tracker_mirror_site))
        found = None
        for file in folder.files:
            if "DNBSEQ_Tracker_Master" in file['Name']:
                found = file
        if found is not None:
            inst_fpath = self.getfile(found['Name'], folder, save_path)
            return inst_fpath, found['TimeLastModified']
        else:
            return None

    def checkin_file(self, file, folder, name):
        print(folder)
        folder = self.site.Folder((folder))
        #folder.check_in(file, comment)
        try:
            with open(file, mode='rb') as file:
                fileContent = file.read()
            folder.upload_file(fileContent, name)
        except Exception as e:
            print(e)
            return str(e)
        return True

    def getfile(self, file, folder, loc):
        file_info = folder.get_file(file)
        print("Downloaded File")
        save_path = os.path.join(loc, file)
        with open(save_path, "wb") as fh:
            fh.write(file_info)
            fh.close()
        print("File Saved")
        return save_path

    def make_folder(self):
        print(self.iruns_site)
        #folder = self.site.Folder(self.iruns_site)
        try:
            response = self.session.post(
                url=self.home_site+ "/_api/web/GetFolderByServerRelativeUrl('" +
                    'Shared Documents/DNBSEQ E tracker/Individual_runs' + "')/folders/add(test)'",
                )
            print(response)
        except Exception as err:
            print("Some error occurred: " + str(err))


class BigUpload:

    def __init__(self, interface, template):
        self.config_template = template
        self.set_inter(interface)


    def set_inter(self, int):

        authcookie = Office365(self.config_template['sp_base_path'], username=int.username,
                               password=int.password).GetCookies()
        self.cookie = authcookie
        session = requests.Session()
        session.cookies = authcookie
        session.headers.update({'user-agent': 'python_tracker'})
        session.headers.update({'accept': 'application/json;odata=verbose'})

        # dirty workaround.... I'm getting the X-RequestDigest from the first failed call
        session.headers.update({'X-RequestDigest': 'FormDigestValue'})
        response = session.post(
            url=self.config_template['sp_base_path'] + "/sites/" + self.config_template['sp_site_name'] +
                "/_api/web/GetFolderByServerRelativeUrl('" + self.config_template['sp_doc_library'] + "')/Files/add(url='a.txt',overwrite=true)",
            data="")
        session.headers.update({'X-RequestDigest': response.headers['X-RequestDigest']})
        self.session = session

    def do_upload(self, data, fname):
        # perform the actual upload
        with open(data, 'rb') as file_input:
            try:
                response = self.session.post(
                    url=self.config_template['sp_base_path'] + "/sites/" +
                        self.config_template['sp_site_name'] + "/_api/web/GetFolderByServerRelativeUrl('" +
                        self.config_template['sp_doc_library'] + "')/"+ self.config_template['sp_path'] + "/add(url='"
                        + fname + "',overwrite=true)",
                    data=file_input)
            except Exception as err:
                print("Some error occurred: " + str(err))
                return False
        return True



if __name__ == '__main__':
    dl_username = 'dlopez@genomics.cn'
    dl_pw = 'wzcyyxblggdfbytg'
    meta_path = os.path.join(os.getcwd(), 'MetaSheets')
    try:
        os.mkdir(meta_path)
    except FileExistsError:
        pass
    try_correct = Plum_Interface(dl_username, dl_pw)
    if try_correct.auth:
        try_correct.connect_site()
        try_correct.import_sysinfo_metadata(meta_path)
    else:
        print("fail")
    try_correct.make_folder()

    #try_incorrect = Plum_Interface(dl_username, '456')
