import os
import pandas as pd
import numpy as np
import re
from RunInfoExtract import RunFolder
from SharePoint_Interface import Plum_Interface
from datetime import datetime

class compare_Run_Sysinfo:

    dnb_row, dnb_id_log_df = None, None
    isc_row, isc_meta_df = None, None
    irc_row, irc_meta_df = None, None
    inst_row, inst_df = None, None

    def __init__(self, runfolder, paths):
        self.runFolder = runfolder
        self.sysinfo_paths = paths
        print(self.sysinfo_paths)
        self.compare_dnb()
        self.compare_isc()
        self.compare_irc()
        self.compare_inst()
        self.return_row = self.return_experimental_input()


    def compare_dnb(self):
        dnb_sample_id_logdf = None
        for file in self.sysinfo_paths.keys():
            if "DNB sample" in file:
                dnb_sample_id_logdf = pd.read_excel(self.sysinfo_paths[file][0],
                                                    sheet_name='DNB sample ID log',
                                                    skiprows=3)
        if dnb_sample_id_logdf is not None:
            self.dnb_id_log_df = dnb_sample_id_logdf.loc[dnb_sample_id_logdf['DNB make Date'].notna(), :]
            #print(self.runFolder.dnb_id)
            id  = self.runFolder.dnb_id

            if re.search(r"null", id, re.IGNORECASE) is not None:
                self.runFolder.dnb_id = 'NULL'
                row = pd.DataFrame(columns=self.dnb_id_log_df.columns)
            else:
                int_id = int("".join(list([val for val in id if val.isnumeric()])))
            #print(int_id)
                row = self.dnb_id_log_df.loc[self.dnb_id_log_df['DNB sample ID'] == int_id, :]
            #print(self.dnb_id_log_df)
            #print(row)
            self.dnb_row = row

    def compare_isc(self):
        isc_meta_df = None
        for file in self.sysinfo_paths.keys():
            if "ISC OQC" in file:
                isc_meta_df = pd.read_excel(self.sysinfo_paths[file][0],
                                            sheet_name='OQC',
                                            skiprows=2, usecols=[x for x in range(1,53)])
        if isc_meta_df is not None:
            self.isc_meta_df = isc_meta_df.loc[isc_meta_df['SN'].notna(), :]
            #print(self.isc_meta_df)
            isc_id = int(self.runFolder.isc_id)
            #print(isc_id)
            row = self.isc_meta_df.loc[self.isc_meta_df['SN'] == isc_id, :]
            if len(row.index.values) > 1:
                row = row.loc[row.index == np.amin(row.index.values), :]
            lot = row['Lot #'].values
            if len(lot) == 0:
                lot_filt = 'NULL'
            else:
                lot_filt = re.sub(r"\D", "", lot[0])
            if self.runFolder.isc_lot is None:
                self.runFolder.isc_lot = lot_filt
            self.isc_row = row

    def compare_irc(self):
        irc_meta_df = None
        for file in self.sysinfo_paths.keys():
            if "IRC OQC" in file:
                irc_meta_df = pd.read_excel(self.sysinfo_paths[file][0],
                                            sheet_name="OQC",
                                            skiprows=2,
                                            usecols=[x for x in range(1, 75)])
        if irc_meta_df is not None:
            self.irc_meta_df = irc_meta_df.loc[irc_meta_df['SN'].notna(), :]
            #print(self.irc_meta_df)
            irc_id = self.runFolder.irc_id
            #print(irc_id)
            row = self.irc_meta_df.loc[self.irc_meta_df['SN'] == irc_id, :]
            if len(row.index.values) > 1:
                row = row.loc[row.index == np.amin(row.index.values), :]
            lot = row['Lot #'].values
            if len(lot) == 0:
                lot_filt = 'NULL'
            else:
                lot_filt = re.sub(r"\D", "", lot[0])
            if self.runFolder.irc_lot is None:
                self.runFolder.irc_lot = lot_filt
            self.irc_row = row
            #print(self.isc_row)

    def compare_inst(self):
        inst_id = self.runFolder.inst_id
        print(inst_id)
        inst_num = re.sub(r"\D", "", inst_id)  #remove all letters
        if "P" in inst_id:
            self.inst_row = None  #set with empty below
            return

        chosen_file = None
        for file in self.sysinfo_paths.keys():
            if "DNBSEQ " in file:
                xl = pd.ExcelFile(self.sysinfo_paths[file][0])
                names = xl.sheet_names
                if inst_num is not None:
                    if inst_num + "#" in names:
                        chosen_file = file
        if chosen_file is not None:
            df = pd.read_excel(self.sysinfo_paths[chosen_file][0],
                               sheet_name=inst_num+"#",
                               skiprows=3)
            self.inst_df = df.loc[df['Start Date'].notna(), :].ffill(axis=0)
            #find date
            latest_idx = self.inst_df.index[-1]

            self.inst_df.loc[latest_idx, ['End date']] = datetime.now().strftime("%Y-%m-%d")
            date = self.runFolder.date

            row = self.inst_df.loc[self.inst_df['Start Date'] < date, :]

            row = row.loc[pd.to_datetime(row['End date'], format="%Y-%m-%d") >= date, :]
            self.inst_row = row

    #link to SP file:  https://bgitech.sharepoint.com/sites/ProductConceptInstrumentBiochemIntegration/Shared%20Documents/DNBSEQ%20E%20tracker/Sys%20Info/ISC/LOT10057.xlsx


    def return_experimental_input(self):

        #if more than one row, select last row, for now
        if self.dnb_row is not None:
            #print(self.runFolder.dnb_id)
            if len(self.dnb_row.index) > 1:
                self.dnb_row = self.dnb_row.loc[self.dnb_row.index == self.dnb_row.index.values[-1], :]
            self.dnb_row.insert(loc=0, column='DNB ID', value=[self.runFolder.dnb_id])
            self.dnb_row.reset_index(drop=True, inplace=True)
            print("DNB", len(self.dnb_row.columns))
            #print(self.dnb_row)
        else:
            self.dnb_row = pd.DataFrame(columns=['DNB sample ID', 'DNB make Date', 'DNB make start time', 'DNB Name',
                                                 'DNB make SOP ID', 'Operator', '"DNA lib Type\n"',
                                                 '"DNA lib Serial number\n"', '"DNA lib Stock in date\n"',
                                                 '"DNA Lib Orig. concentration\n"', 'DNA Lib Expired date ',
                                                 "DNA Lib Open'd date", 'DNB Make kit product  name', 'DNB make serial number',
                                                 'Stock in date ', 'DNB make kit expiration date', '"DNB make Open\'d date\n"',
                                                 'Qbit batch ID ', '"Initial DNA quantitation (ng/ul) \n"', 'Initial DNA lib amount (fmol)',
                                                 '"DNB make time (min)\n"', 'DNB make final concentration(ng/ul', 'DNB aliquote tube numbers ',
                                                 'DNB aliuqoted volume per tube(ul)', 'Pass/Fail ', 'Other comment '])
            self.dnb_row.insert(loc=0, column='DNB ID', value=[self.runFolder.dnb_id])

        if self.isc_row is not None:
            #print(self.isc_row.columns)
            if len(self.isc_row.index) > 1:
                self.isc_row = self.isc_row.loc[self.isc_row.index == self.isc_row.index.values[-1], :]
            self.isc_row.insert(loc=0, column='ISC ID', value = [self.runFolder.isc_id])
            self.isc_row.reset_index(drop=True, inplace=True)
            print('ISC', len(self.isc_row.columns))
            #print(self.isc_row)
        else:
            self.isc_row = pd.DataFrame(columns=['Mfg Date', 'Lot #', 'SN', 'Process code', 'wafer ID', 'Sensor ID',
                                                 '"Sensor \nlocation"', 'LGA ID', '去胶位置', 'uF ID', '左接触角 (deg)',
                                                 '"右接触角\n(deg)"', '接触角判定', '"FQC Loading \n(%)"', '"FQC NSB \n(%)"',
                                                 'FQC 判定', '"LGA下左偏移\n (um)"', '"LGA下右偏移 \n(um)"',
                                                 '"LGA左右差异\n (<=90um)"', 'LGA 偏移判定', '"Seal ring 高度 \n(um)"',
                                                 '"flowcell 高度\n (um)"', '胶层厚度(um)', '胶线工艺判定', '漏气 (<0.2 psi 10sec)',
                                                 '气密性判定', '电性能(dealine,  imagize detectivity, see folder for details)',
                                                 '电性能判定', '外观胶线检验结果( glueline, gule dispensing)', '外观标签，条形码，包装的检验',
                                                 '外观判定', 'SQC SOP版本号', ' Loading (%)', 'SQC NSB (%)', 'Mapping rate%',
                                                 'AveErrorRate%', 'ESR %', 'InitialQ30%', 'InitialBIC%', 'Lag%', 'Runon%',
                                                 'Initial G signal', 'Initial N signal', 'TotalRead（M）', '测序性能总判定',
                                                 '芯片总判定', 'ISC Output Assignment', '备注'])
            self.isc_row.insert(loc=0, column='ISC ID', value=[self.runFolder.isc_id])
        if self.runFolder.isc_lot != "NULL":
            self.isc_row.loc[:, 'Lot #'] = self.runFolder.isc_lot

        if self.irc_row is not None:
            #print(self.irc_row)
            #print(self.isc_row.columns)
            if len(self.irc_row.index) > 1:
                self.irc_row = self.irc_row.loc[self.irc_row.index == self.irc_row.index.values[-1], :]
            self.irc_row.insert(loc=0, column='IRC ID', value=[self.runFolder.irc_id])
            self.irc_row.reset_index(drop=True, inplace=True)
            print('IRC', len(self.irc_row.columns))
            #print(self.irc_row.columns)
        else:
            self.irc_row = pd.DataFrame(columns=['Mfg Date', 'Lot #', 'SN', '试剂盒规格', 'Mfg Process Version',
                                                 '空试剂盒批次', '橡胶塞批次', '壳体批次', '下盖批次', '试剂袋批次', '试剂袋外接头批次',
                                                 '密封垫批次', '接头座批次', '穿刺上盖批次', '热封铝膜批次', '硅胶管批次', 'BP1 批次',
                                                 'BP2 批次', 'MP 批次', 'MB 批次', 'IP2 批次', 'Phi29 DNA polymerase (小管)',
                                                 'MDA buffer (小管)', 'BR批次', 'DRB 批次', 'PWB 批次', 'DCB 批次', 'IP1 批次',
                                                 'DLB 批次 (小管)', 'GWB 批次', 'RR 批次', 'SR 批次', 'CR 批次', 'N 底物批次',
                                                 'G 底物批次', 'BE Stock批次 (小管)', '试剂槽灌装日期', '试剂槽灌装方式', '试剂槽自动灌装脚本',
                                                 'N底物灌装日期', 'G底物灌装日期', '试剂袋灌装方式', 'N底物灌装量/ml', 'G底物灌装量/ml',
                                                 '试剂袋安装位置正确', '试剂袋外接头安装到位且密封垫未脱落', '试剂袋外接头安装方向正确',
                                                 '试剂袋胶管连接未断开无泄漏；输液夹夹紧', '试剂袋无泄漏', '橡胶塞无泄露/无杂质', '密封铝膜完整无破损',
                                                 '下盖不脱落/无杂质', '接头座O型圈已安装/管路连接正确', '接头座已用插针固定', '接头座密封垫未脱落', 'N底物起始亮度',
                                                 'G底物起始亮度', 'Average error rate (%)', 'Mapping rate (%)', 'Lag (%)', 'Runon (%)',
                                                 'ESR (%)', 'Q30 (%)', 'Loading rate (%)', 'NSB (%)', 'Mapped Reads(M)', 'C01_BIC(%)',
                                                 'C01_FIT(%)', 'C01_ESR(%)', 'C01_Q30(%)', 'SQC判定', '产品分类', '备注'])
            self.irc_row.insert(loc=0, column='IRC ID', value = [self.runFolder.irc_id])
        if self.runFolder.irc_lot != 'NULL':
            self.irc_row.loc[:, 'Lot #'] = self.runFolder.irc_lot

        if self.inst_row is not None:
            #print(self.inst_row)
            if len(self.inst_row.index) > 1:

                self.inst_row = self.inst_row.loc[self.inst_row.index == self.inst_row.index.values[-1], :]
            self.inst_row.insert(loc=0, column='Instrument ID', value=[self.runFolder.inst_id])
            self.inst_row.reset_index(drop=True, inplace=True)
            print('INST', len(self.inst_row.columns))
        else:
            self.inst_row = pd.DataFrame(columns=['Event ID', 'Owner', 'Issue type', 'Instrument status', 'Start Date',
                                                  'End date', 'Recording document ID', 'Instrument Type',
                                                  'DNBSEQ E version', 'SW_Ecoli Sequence', 'SW_Ecoli Ctrl(MCU)',
                                                  'SW_Ecoli Ctrl(FPGA)', 'SW_Temperature(MCU)', 'SW_Ecoli Ctrl(CMOS) ',
                                                  'SW_LINUX', 'Shell', 'EE_Controller board', 'EE_Driver board',
                                                  'EE_CMOS  Interface board', 'EE_Power Board', 'EE_USB WLAN converter board',
                                                  '\nEE_Image acquisition board', 'Server ID', 'Server software version ',
                                                  'Basecall version ', 'Performance QC SOP ID', 'MappedReads(M)',
                                                  'Loading rate(%)', 'Q30 (%)', 'Lag(%)', 'Runon(%)', 'MappingRate(%)',
                                                  'AvgErrorRate(%)', 'Notes'])
            self.inst_row.insert(loc=0, column='Instrument ID', value=[self.runFolder.inst_id])
            print('INST', len(self.inst_row.columns))

        dnb_cols = self.dnb_row.columns.values
        isc_cols = self.isc_row.columns.values
        irc_cols = self.irc_row.columns.values
        inst_cols = self.inst_row.columns.values
        df_list = [self.dnb_row, self.isc_row, self.irc_row, self.inst_row]
        col_list = [list(x.columns.values) for x in df_list]
        #col_list = [dnb_cols, isc_cols, irc_cols, inst_cols]
        prepend_values = ['DNB ', 'ISC ', 'IRC ', 'Inst. ']
        df_list_out = []
        for prep_col_str, df in zip(prepend_values, df_list):
            cur_cols = col_list.pop(0)
            other_cols = []
            for i in range(len(col_list)):
                other_cols.extend(col_list[i])
            #print(set(cur_cols), set(other_cols))
            dup_cols = set(cur_cols) & set(other_cols)
            #print(dup_cols)
            rename_cols = {}
            for dup in dup_cols:
                rename_cols[dup] = prep_col_str + dup
            #print(prep_col_str, dup_cols)
            rename_df = df.rename(columns=rename_cols)
            df_list_out.append(rename_df)
            col_list.append(cur_cols)
        #remove duplicate column names
        show_row_cc = []
        for i in range(len(df_list_out)):
            df = df_list_out[i]
            first_cols = df.iloc[[0], [0,1,2,3,4,5]]
            show_row_cc.append(first_cols)
        return_row = pd.concat(df_list_out, axis=1)
        self.show_row = show_row_cc
        #print(return_row)
        #print(return_row.index)
        #print(return_row.columns)
        #print(len(return_row.columns))
        return return_row


if __name__ == '__main__':
    cwd = os.getcwd()
    print(cwd)
    dirs = next(os.walk(cwd))[1]
    print(dirs)
    select_dir = dirs[4]
    start_path = os.path.join(cwd, select_dir)
    save_path = os.path.join(cwd, 'Save_folder')
    run = RunFolder(start_path, save_path)
    run.init_extract()

    dl_username = 'dlopez@genomics.cn'
    dl_pw = 'wzcyyxblggdfbytg'
    meta_path = os.path.join(os.getcwd(), 'MetaSheets')
    try:
        os.mkdir(meta_path)
    except FileExistsError:
        pass
    try_correct = Plum_Interface(dl_username, dl_pw)
    paths = None
    if try_correct.auth:
        try_correct.connect_site()
        paths = try_correct.import_sysinfo_metadata(meta_path)
        #print("paths:", paths)
    else:
        print("fail")
    if paths is not None:
        compare = compare_Run_Sysinfo(run, paths)