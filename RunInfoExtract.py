import os
import pandas as pd
import numpy as np
from cv2 import normalize, NORM_MINMAX, imwrite
from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.text import PP_PARAGRAPH_ALIGNMENT
from pptx.enum.shapes import MSO_AUTO_SHAPE_TYPE
from math import ceil
import matplotlib.pyplot as plt
from xlsxwriter.utility import xl_col_to_name, xl_range
from pptx.dml.color import RGBColor
import zipfile
import re
import sys

class RunFolder:

    html_text = None
    date = None
    isc_id = None
    isc_lot = None
    irc_id = None
    irc_lot = None
    inst_id = None
    dnb_id = None

    html_file = None

    area_info_df = None
    perc_area_info_df = None

    scatter_dir = None
    log_dir = None
    rawimg_dir = None

    def __init__(self, path, slide_save_folderpath):
        self.save_folderpath = slide_save_folderpath
        self.folderpath = path
        self.dirname = path.split("\\")[-1]

    def init_extract(self):

        self.extract_summRep_html()
        self.extract_testarea_info()
        self.find_folders()
        self.extract_info_from_dirname()
        print(self.date, self.isc_id, self.irc_id, self.inst_id, self.dnb_id)
        #self.make_a_slide("test_call_build0", {})

    def finalize_params(self, run_name):
        run_split = run_name.split("_")
        self.date = run_split[0]
        self.isc_id = run_split[1]
        self.isc_lot = run_split[2]
        self.irc_id = run_split[3]
        self.irc_lot = run_split[4]
        self.inst_id = run_split[5]
        self.dnb_id = run_split[6]

    def extract_summRep_html(self):
        html = [x for x in os.listdir(self.folderpath) if x.endswith(".html")]
        txt_fs = [x for x in os.listdir(self.folderpath) if x.endswith(".txt")]
        print(html)
        if len(html) > 1:
            print("Multple HTML in Directory")
        elif len(html) == 0:
            pass
        else:
            basecall_txt = None
            for txt in txt_fs:
                if re.search(r"BasecallQC", txt, re.IGNORECASE) is not None:
                    basecall_txt = txt
            if basecall_txt is not None:
                self.html_file = summaryReportHTML(os.path.join(self.folderpath, html[0]),
                                                   os.path.join(self.folderpath, basecall_txt))  #needs refine
                self.html_file.output_plots(self.save_folderpath)
            else:
                self.html_file = summaryReportHTML(os.path.join(self.folderpath, html[0]))  # needs refine
                self.html_file.output_plots(self.save_folderpath)

    def make_html_dfrow(self, se1, pe1):
        if self.html_file is None:
            return pd.DataFrame()
        else:
            return self.html_file.make_html_dfrow(se1, pe1)

    def extract_info_from_dirname(self):
        split = self.dirname.split("/")[-1].split("_")
        for i in range(len(split)):
            if i == 0:#date
                self.date = split[i]
            elif i == 1:#isc_id
                self.isc_id = split[i][-9:]  #last 9 chars
            elif i == 2:#isc_lot
                self.isc_lot = split[i]
            elif i == 3:#irc_id
                self.irc_id = split[i]
            elif i == 4:#irc_lot
                self.irc_lot = split[i]
            elif i == 5:#inst_id
                self.inst_id = split[i].replace("#", "")  #ensure non # errors
            elif i == 6:#dnb_id
                self.dnb_id = split[i]
            else:
                pass

        if self.html_file is not None:
            summary_table_dict = self.html_file.summaryTable
            #pull IDs, fill from sysinfo
            select_keys = ['FlowcellID', 'ReagentID', 'MachineID', 'DNBID']
            # check if all ids are available, ie htmlv2.4.0
            # set all params then return
            if all(named_id in summary_table_dict for named_id in select_keys):
                if self.isc_id is None:
                    self.isc_id = summary_table_dict['FlowcellID'][-9:] #last 9 chars
                if self.irc_id is None:
                    self.irc_id = summary_table_dict['ReagentID']
                if self.inst_id is None:
                    imp_inst_id = summary_table_dict['MachineID']
                    if imp_inst_id == 'NULL':
                        self.inst_id = imp_inst_id
                    else:
                        self.inst_id = "G" + imp_inst_id    #add G if imported from html, not NULL
                if self.dnb_id is None:
                    self.dnb_id = summary_table_dict['DNBID']
                return
            else:
                #Missing sysinfo in html, <htmlv2.4.0
                pass

        print(self.date, self.isc_id, self.isc_lot, self.irc_id, self.irc_lot, self.inst_id, self.dnb_id)
        print("Extract Done")

    def return_update_paraminfo(self, update_dict=None):
        param_keys = ["Test Date", "ISC id", "ISC Lot", "IRC id", "IRC Lot", "Instrument id", "DNB id"]
        param_order = [self.date, self.isc_id, self.isc_lot, self.irc_id, self.irc_lot, self.inst_id, self.dnb_id]
        param_dict = {}
        for pkey, p_var in zip(param_keys, param_order):
            param_dict[pkey] = p_var
        if update_dict is None:
            return param_dict
        else:
            self.date = update_dict['Test Date']
            self.isc_id = update_dict['ISC id']
            self.isc_lot = update_dict['ISC Lot']
            self.irc_id = update_dict['IRC id']
            self.irc_lot = update_dict['IRC Lot']
            self.inst_id = update_dict['Instrument id']
            self.dnb_id = update_dict['DNB id']

    def extract_testarea_info(self):
        if os.path.isfile(os.path.join(self.folderpath, "test_area_analysis.csv")):
            area_info_df = pd.read_csv(os.path.join(self.folderpath, "test_area_analysis.csv"), skiprows=6, skipfooter=3, engine='python')
            #area_info_df.drop(columns=["xmin","xmax","ymin","ymax","NonspecificAdsorptionNum"])

            skiprows = 6+len(area_info_df.index)+2
            perc_area_info = pd.read_csv(os.path.join(self.folderpath, "test_area_analysis.csv"), skiprows=skiprows)

            self.area_info_df = area_info_df.drop(columns=["xmin","xmax","ymin","ymax","NonspecificAdsorptionNum"])
            self.perc_area_info_df = perc_area_info

            #print(self.area_info_df, self.perc_area_info_df)
            #self.make_testarea_dfrow()

    def find_folders(self):
        dirs = next(os.walk(self.folderpath))[1]
        print(dirs)
        new_dir = os.path.join(self.folderpath, 'ScaledImages')
        try:
            os.mkdir(new_dir)
        except FileExistsError:
            print("ScaledImages already Exists")
        self.scaled_image_dir = new_dir
        for d in dirs:
            if re.search(r'scatter', d, re.IGNORECASE) is not None:
                print("Scatter Folder:  %s"%d)
                self.scatter_dir = scatter_Folder(os.path.join(self.folderpath, d))
            elif re.search(r'log', d, re.IGNORECASE) is not None:
                print("Operation Log Folder:  %s"%d)
                self.log_dir = operationlog_Folder(os.path.join(self.folderpath, d))
            elif re.search(r'raw', d, re.IGNORECASE) is not None:
                print("Raw Image Folder:  %s"%d)
                self.rawimg_dir = rawImage_Folder(os.path.join(self.folderpath, d), self.scaled_image_dir)
                self.rawimg_df = self.rawimg_dir.output_images()
            elif re.search(r"ScaledImages", d, re.IGNORECASE) is not None:
                continue
            elif re.search(r"scripts", d, re.IGNORECASE) is not None:
                self.script_folder = d
            else:
                print("unknown folder:  %s"%d)

    def make_a_slide(self, file_name, lbar_dict):
        """
        Makes a pptx presentation slide summary

        """
        selec_img = None
        if self.rawimg_dir is not None:
            selected_min_cyclnum = self.rawimg_df.loc[self.rawimg_df['Cycle Number'] == np.amin(self.rawimg_df['Cycle Number'].values), :]
            if len(selected_min_cyclnum.index) > 1:
                selec_img = selected_min_cyclnum.loc[selected_min_cyclnum['Image Name'].str.contains("Signal_Img1")]
                if len(selec_img.index) > 1:
                    selec_img = selec_img.iloc[np.amin(selec_img.index.values), :]
            else:
                selec_img = selected_min_cyclnum
        #print(selec_img)
        pres = runSlidePresentation(os.path.join(self.folderpath, file_name+".pptx"), self.save_folderpath)
        if self.html_file is not None:
            html_table = self.html_file.summaryTable
            if self.perc_area_info_df is not None:
                html_table['Filtered NSB(%)'] = str(round(self.perc_area_info_df['filtered NSB(%)'].values[0], 2))
            pres.add_table(html_table)
        if self.scatter_dir is not None:
            scatter_files_paths = self.scatter_dir.first_last_scatter()
            pres.add_scatter(scatter_files_paths)
        if self.html_file is not None:
            pres.add_plots(self.html_file.temp_plot_paths)
        pres.add_lbar(lbar_dict)
        if selec_img is not None:
            pres.add_scale_img(selec_img['Scale Path'].values[0])
        pres.save_prs()

    def make_testarea_dfrow(self):
        if self.area_info_df is not None:
            self.area_info_df['TS Cols'] = ['TestArea_%s'%x for x in range(1, len(self.area_info_df.index.values)+1)]
            #print(self.area_info_df)

            nsbp_row = self.area_info_df.loc[:, ['TS Cols', 'NonspecificAdsorptionPct(%)']]
            nsbp_row['NSA ColNames'] = "NonspecificAdsorptionPct(%)\n" + nsbp_row['TS Cols'].astype(str)
            nsbp_row.drop('TS Cols', axis=1, inplace = True)
            nsb_col = nsbp_row.set_index('NSA ColNames', drop=True).T
            nsb_col.reset_index(drop=True, inplace=True)

            nsb_stats = self.perc_area_info_df.loc[:, ['NSB(%)', 'NSB STD(%)', 'filtered NSB(%)', 'filtered NSB STD(%)', 'Test Area Filtered Out']]
            nsb_stats.reset_index(drop = True, inplace = True)

            nsb_col = pd.concat([nsb_col, nsb_stats], axis = 1)

            if 'DNB loading rate(%)' in self.area_info_df.columns:
                nsbp_row = self.area_info_df.loc[:, ['TS Cols', 'TestAreaID']]
                nsbp_row['LR ColNames'] = "DNB loading rate(%)\n" + nsbp_row['TS Cols'].astype(str)
                nsbp_row.drop('TS Cols', axis=1, inplace=True)
                lr_col = nsbp_row.set_index('LR ColNames', drop=True).T
                lr_col.reset_index(drop=True, inplace=True)

                lr_stats = self.perc_area_info_df.loc[:,
                           ~self.perc_area_info_df.columns.isin(['NSB(%)', 'NSB STD(%)', 'filtered NSB(%)',
                                                                 'filtered NSB STD(%)', 'Test Area Filtered Out'])].reset_index(drop=True, inplace=True)
                lr_col = pd.concat([lr_col, lr_stats], axis=1)

                test_area_dfrow = pd.concat([nsb_col, lr_col], axis=1)

            else:
                test_area_dfrow = nsb_col
        else:
            test_area_dfrow = pd.DataFrame()

        return test_area_dfrow

    def zip_runfolder(self, zip_save_dir, zipf_name):
        ignore_dirs = []
        if self.rawimg_dir is not None:
            ignore_dirs.append(self.rawimg_dir.outer_path.split("\\")[-1])
        if self.log_dir is not None:
            ignore_dirs.append(self.log_dir.folder_path.split("\\")[-1])
        if self.rawimg_dir is not None:
            ignore_dirs.append("ScaledImages")
        run_f_path = self.folderpath
        zip_paths = []
        try:
            for i in range(len(ignore_dirs)+1):
                if i == 0:
                        zf1_name = zip_save_dir+"_0.zip"
                        zip_file_one = zipfile.ZipFile(zf1_name, 'w', zipfile.ZIP_DEFLATED)
                        ZipDir(run_f_path, zip_file_one, ignoreDir=ignore_dirs, ignoreExt=[])
                        zip_file_one.close()
                        zip_paths.append([zf1_name, zipf_name+"_0.zip"])
                else:
                    zfn_name = zip_save_dir+"_%s.zip"%str(i)
                    zip_file_n = zipfile.ZipFile(zfn_name, 'w', zipfile.ZIP_DEFLATED)
                    ign_path = os.path.join(run_f_path, ignore_dirs[i-1])
                    zipIgnoreDir(ign_path, run_f_path, zip_file_n)
                    zip_file_n.close()
                    zip_paths.append([zfn_name,zipf_name+"_%s.zip"%str(i)])

        except IOError as message:
            print(message)
            return False, None
        except OSError as message:
            print(message)
            return False, None
        except zipfile.BadZipfile as message:
            print(message)
            return False, None
        finally:
            print("zip complete")
            return True, zip_paths






class runSlidePresentation:

    width = 13.333
    height = 7.5

    col_width = width/6
    row_height = height/3

    font_type = 'Arial'

    def __init__(self, out_file_path_name, temp_folder_path):
        self.tempfolder = temp_folder_path
        self.out_file_path_name = out_file_path_name
        slide_pres = Presentation()
        slide_pres.slide_width = Inches(self.width)
        slide_pres.slide_height = Inches(self.height)

        layout = slide_pres.slide_layouts[6]  # blank slide layout
        self.slide = slide_pres.slides.add_slide(layout)

        self.slide_pres = slide_pres

    def add_table(self, summary_report_dict):
        table_height = self.row_height
        table_width = self.col_width*3
        #print(table_height, table_width)

        num_rows = ceil(len(summary_report_dict.keys())/4)*2

        if len(summary_report_dict.keys()) < (num_rows / 2 * 4):
            diff = int((num_rows / 2 * 4) - len(summary_report_dict.keys()))
            #print("diff", diff)
            for i in range(diff):
                summary_report_dict["_%s"%i] = None

        #num_rows = ceil(len(summary_report_dict.keys())/2)
        #print(summary_report_dict, len(summary_report_dict.keys()))
        #print(num_rows)
        x, y, cx, cy = Inches(self.col_width), Inches(0), Inches(table_width), Inches(table_height)
        shape = self.slide.shapes.add_table(num_rows, 4, x, y, cx, cy)
        table = shape.table
        table.first_row = False

        fontsize = 12
        if num_rows > 9:
            fontsize = 8

        for index, loc_tup in zip(summary_report_dict.keys(), [(r,c) for r in range(0, int(num_rows), 2) for c in range(4)]):
            if "_" not in index:
                title_text = index
                value_text = summary_report_dict[index]
            else:
                title_text = " "
                value_text = " "
            #print(loc_tup, title_text, value_text)
            cell = table.cell(loc_tup[0], loc_tup[1])
            p = cell.text_frame.paragraphs[0]
            run = p.add_run()
            run.text = title_text
            font = run.font
            font.name = self.font_type
            font.size = Pt(fontsize)
            font.bold = True
            font.color.rgb = RGBColor(63, 5, 81)
            cell = table.cell(loc_tup[0]+1, loc_tup[1])
            p = cell.text_frame.paragraphs[0]
            run = p.add_run()
            run.text = value_text
            font = run.font
            font.name = self.font_type
            font.size = Pt(fontsize)

    def add_scatter(self, scatter_file_paths):
        if len(scatter_file_paths) != 2:
            print("Incorrect # of Scatter Plots")
        else:
            img = self.slide.shapes.add_picture(scatter_file_paths[0], Inches(self.col_width*4), Inches(0),
                                                height=Inches(self.row_height*0.88))
            txtbox_1 = self.slide.shapes.add_textbox(Inches(self.col_width*4), Inches(0), Inches(2.1), Inches(0.5))
            p = txtbox_1.text_frame.paragraphs[0]
            run = p.add_run()
            run.text = 'Scatter_C%s'%scatter_file_paths[0].split("//")[-1].split("_")[-1][:-4]
            font = run.font
            font.name = self.font_type
            font.size = Pt(16)
            p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER
            img = self.slide.shapes.add_picture(scatter_file_paths[1], Inches(self.col_width * 5), Inches(0),
                                                height=Inches(self.row_height*0.88))
            txtbox_2 = self.slide.shapes.add_textbox(Inches(self.col_width*5), Inches(0), Inches(2.1), Inches(0.5))
            p = txtbox_2.text_frame.paragraphs[0]
            run = p.add_run()
            run.text = 'Scatter_C%s'%scatter_file_paths[1].split("//")[-1].split("_")[-1][:-4]
            font = run.font
            font.name = self.font_type
            font.size = Pt(16)
            p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER

    def add_plots(self, plot_path_dict):
        column = 1
        row = 1
        text_box_scale = 0.9
        font_size = 16
        row_shift_factor = 0.2
        for plot in plot_path_dict.keys():
            plot_dir = plot_path_dict[plot]
            img = self.slide.shapes.add_picture(plot_dir, Inches(self.col_width * column), Inches(self.row_height*(row+row_shift_factor)),
                                                height=Inches(self.row_height * 0.9))
            remove_textbox = """
            if plot == 'Mapped Base Distribution':
                #text_box_scale = 0.89999
                font_size = 14
            txtbox_1 = self.slide.shapes.add_textbox(Inches(self.col_width * column), Inches(self.row_height*row*text_box_scale),
                                                     Inches(2.1), Inches(0.5))
            p = txtbox_1.text_frame.paragraphs[0]
            txtbox_1.text_frame.word_wrap = True
            run = p.add_run()
            run.text = plot
            font = run.font
            font.name = self.font_type
            font.size = Pt(font_size)
            p.alignment = PP_PARAGRAPH_ALIGNMENT.CENTER"""
            column += 1
            if column == 6:
                column = 1
                row = 2
                row_shift_factor = 0.1
                text_box_scale = 0.95

    def add_lbar(self, lbar_dict):

        shapes = self.slide.shapes
        height, width, x, y = Inches(self.height), Inches(self.col_width), Inches(0), Inches(0)
        shape = shapes.add_shape(MSO_AUTO_SHAPE_TYPE.RECTANGLE, x, y, width, height)
        fill = shape.fill
        fill.solid()
        fill.fore_color.rgb = RGBColor(148, 131, 177)
        y_spacer = 0.0667*self.height
        y_start = 0.0133*self.height

        date_txtbox = self.slide.shapes.add_textbox(Inches(0.1), Inches(y_start), Inches(self.col_width), Inches(self.row_height/4))
        p = date_txtbox.text_frame.paragraphs[0]
        run = p.add_run()
        run.text = lbar_dict['Date']
        font = run.font
        font.name = self.font_type
        font.size = Pt(16)

        y_start += y_spacer

        name_textbox = self.slide.shapes.add_textbox(Inches(0.1), Inches(y_start), Inches(self.col_width), Inches(self.row_height/4))
        p = name_textbox.text_frame.paragraphs[0]
        run = p.add_run()
        run.text = lbar_dict['Name']
        font = run.font
        font.name = self.font_type
        font.size = Pt(16)
        font.bold = True
        name_textbox.text_frame.word_wrap=True

        y_start += y_spacer
        double_keys = ['Instr', 'Instr ID', 'DNBSEQ E Ver', 'SW Ecoli Seq', 'Seq Prep. SOP Ver.', 'IRC Lot', 'ISC Lot', 'DNB ID']
        for key in double_keys:
            instr_textbox = self.slide.shapes.add_textbox(Inches(0.02), Inches(y_start), Inches(self.col_width), Inches(self.row_height/4))
            p = instr_textbox.text_frame.paragraphs[0]
            run = p.add_run()
            run.text = key
            font = run.font
            font.name = self.font_type
            font.size = Pt(10)
            font.color.rgb = RGBColor(255, 255, 255)
            font.bold = True
            run = p.add_run()
            run.text = "\n"+str(lbar_dict[key])
            font = run.font
            font.name = self.font_type
            font.size = Pt(14)
            y_start += y_spacer
        y_start += 0.65*self.row_height
        se_pass_tb = self.slide.shapes.add_textbox(Inches(0.02), Inches(y_start), Inches(self.col_width), Inches(self.row_height/4))
        p = se_pass_tb.text_frame.paragraphs[0]
        run = p.add_run()
        run.text = 'SE Result'
        font = run.font
        font.name = self.font_type
        font.size = Pt(10)
        font.color.rgb = RGBColor(255, 255, 255)
        font.bold = True
        run = p.add_run()
        run.text = "\n"+str(lbar_dict['SE Result'])
        font = run.font
        font.name = self.font_type
        font.size = Pt(14)
        pe_pass_tb = self.slide.shapes.add_textbox(Inches(0.55*self.col_width), Inches(y_start), Inches(self.col_width),
                                                   Inches(self.row_height / 4))
        p = pe_pass_tb.text_frame.paragraphs[0]
        run = p.add_run()
        run.text = 'PE Result'
        font = run.font
        font.name = self.font_type
        font.size = Pt(10)
        font.color.rgb = RGBColor(255, 255, 255)
        font.bold = True
        run = p.add_run()
        run.text = "\n" + str(lbar_dict['PE Result'])
        font = run.font
        font.name = self.font_type
        font.size = Pt(14)


    def add_scale_img(self, scale_img_path):

        shapes = self.slide.shapes
        picture = shapes.add_picture(scale_img_path, Inches(0.02), Inches(2.1*self.row_height), Inches(2.02), Inches(1.52))
        #picture.size = (400, 400)
        #start_y = 5.1

    def save_prs(self):
        self.slide_pres.save(self.out_file_path_name)


class summaryReportHTML:

    darkimage_results = None
    imgsignal = None
    params = {'legend.fontsize': 'x-large',
              'figure.figsize': (15, 5),
              'axes.labelsize': 'x-large',
              'axes.titlesize': 'xx-large',
              'xtick.labelsize': 'x-large',
              'ytick.labelsize': 'x-large'}

    colors = ['red', 'green', 'gold', 'blue']

    mark = ["o", "o", "^", "s"]
    linestyles = ['solid', 'dashed']

    temp_plot_paths = {}

    def __init__(self, path_to_html, path_to_txt=None):
        self.htmlpath = path_to_html
        with open(self.htmlpath, 'rb') as htmlf:
            htmlbyte = htmlf.read()
            self.html_text = str(htmlbyte)
            htmlf.close()
        self.extract_content_htmlstr()
        plt.rcParams.update(self.params)
        #self.make_html_dfrow(1, 50)

    def extract_content_htmlstr(self):

        self.extract_darkimages()
        self.extract_imsignal()
        self.extract_seqSNR()
        self.extract_varbic()
        self.extract_q30()
        self.extract_runon()
        self.extract_lag()
        self.extract_baseTypeDist()
        self.extract_discordance()

        self.extract_im_biobk()
        self.extract_im_snr()
        self.extract_SummaryTable()

        #self.print_extract_data()

    def print_extract_data(self):
        print("Dark Images: 2")
        print(self.darkimage_results)
        print("Image Signal: 2")
        print(self.imgsignal)
        print("Image SNR: 2")
        print(self.im_snr)
        print("Seq SNR: 2")
        print(self.seqsnr)
        print("Biobk: 2")
        print(self.im_biobk)
        print("BIC: 1")
        print(self.bic)
        print("Q30: 1")
        print(self.q30)
        print("Runon: 3")
        print(self.runon) #1, 2, avg
        print("Lag: 3")
        print(self.lag) #1,2,avg
        print("BaseDistribution: A,C,T,G,N")
        print(self.basetype_dist)
        print("Discordance: 3")  #mismatch, mismatchNoN, gap
        print(self.discordance)
        print(self.summaryTable)

    def extract_darkimages(self):
        idx0 = self.html_text.find("var im_dark")
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find(']};')

        im_dark2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        im_dark2 = np.array([float(x) for x in im_dark2.split(", ")])
        im_dark1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        im_dark1 = np.array([float(x) for x in im_dark1.split(", ")])

        self.darkimage_results = {"Image1": im_dark1, "Image2": im_dark2}

    def extract_imsignal(self):
        # find 'var im_signal'
        idx0 = self.html_text.find('var im_signal')
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find(']};')

        # output
        im_signal2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        im_signal2 = np.array([float(x) for x in im_signal2.split(", ")])
        im_signal1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        im_signal1 = np.array([float(x) for x in im_signal1.split(", ")])
        self.imgsignal = {"Image1":im_signal1, "Image2":im_signal2}

    def extract_im_biobk(self):
        # find 'var im_biobk'
        idx0 = self.html_text.find("var im_biobk")
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find(']};')

        # output
        im_biobk2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        im_biobk2 = np.array([float(x) for x in im_biobk2.split(", ")])
        im_biobk1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        im_biobk1 = np.array([float(x) for x in im_biobk1.split(", ")])
        self.im_biobk = {"Image1":im_biobk1, "Image2":im_biobk2}

    def extract_im_snr(self):
        # find 'var im_snr'
        idx0 = self.html_text.find('var im_snr')
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find(']};')

        # output
        imsnr2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        imsnr2a = np.array([float(x) for x in imsnr2.split(", ")])
        imsnr1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        imsnr1a = np.array([float(x) for x in imsnr1.split(", ")])
        self.im_snr = {"Image1":imsnr1a, "Image2":imsnr2a}

    def extract_seqSNR(self):
        # find 'var snr' equivalent to SeqSNR
        idx0 = self.html_text.find('var snr')
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find(']};')

        # output
        seqsnr2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        seqsnr2 = np.array([float(x) for x in seqsnr2.split(", ")])
        seqsnr1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        seqsnr1 = np.array([float(x) for x in seqsnr1.split(", ")])
        self.seqsnr = {"Image1":seqsnr1, "Image2":seqsnr2}

    def extract_varbic(self):
        # find 'var bic'

        idx0 = self.html_text.find('var bic')
        idx1 = self.html_text[idx0:].find('{"BIC": [')
        idx2 = self.html_text[idx0:].find(']')

        # output
        bic = self.html_text[(idx0 + idx1 + len('{"BIC": [')):idx0 + idx2]
        self.bic = {"BIC":np.array([float(x) for x in bic.split(", ")])}

    def extract_q30(self):
        # find 'var q30'
        idx0 = self.html_text.find('var q30')
        idx1 = self.html_text[idx0:].find('{"Q30": [')
        idx2 = self.html_text[idx0:].find(']};')

        # output
        Q30 = self.html_text[(idx0 + idx1 + len('{"Q30": [')):idx0 + idx2]
        self.q30 = {"Q30":np.array([float(x) for x in Q30.split(",")])}

    def extract_runon(self):
        # find 'var runon ='
        idx0 = self.html_text.find('var runon =')
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find('], "AVG": [')
        idx4 = self.html_text[idx0:].find(']};')

        # output
        runon2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        runon2a = np.array([float(x) for x in runon2.split(", ")])
        runon1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        runon1a = np.array([float(x) for x in runon1.split(", ")])
        runonAvg = self.html_text[(idx0 + idx3 + len('{"image2": [')):idx0 + idx4]
        runonAvga = np.array([float(x) for x in runonAvg.split(", ")])
        self.runon = {"Runon1":runon1a, "Runon2":runon2a, "RunonAvg":runonAvga} #1, 2, avg

    def extract_lag(self):
        # find 'var lag ='
        idx0 = self.html_text.find('var lag =')
        idx1 = self.html_text[idx0:].find('{"image2": [')
        idx2 = self.html_text[idx0:].find('], "image1": [')
        idx3 = self.html_text[idx0:].find('], "AVG": [')
        idx4 = self.html_text[idx0:].find(']};')

        # output
        lag2 = self.html_text[(idx0 + idx1 + len('{"image2": [')):idx0 + idx2]
        lag2a = np.array([float(x) for x in lag2.split(", ")])
        lag1 = self.html_text[(idx0 + idx2 + len('{"image1": [') + 2):idx0 + idx3]
        lag1a = np.array([float(x) for x in lag1.split(", ")])
        lagAvg = self.html_text[(idx0 + idx3 + len('{"image2": [')):idx0 + idx4]
        lagAvga = np.array([float(x) for x in lagAvg.split(", ")])
        self.lag = {"Lag1":lag1a, "Lag2":lag2a, "LagAvg":lagAvga}  #1, 2, avg

    def extract_baseTypeDist(self):
        # find 'var baseTypeDist'
        idx0 = self.html_text.find('var baseTypeDist')
        idx1 = self.html_text[idx0:].find('{"A": [')
        idx2 = self.html_text[idx0:].find('], "C": [')
        idx3 = self.html_text[idx0:].find('], "T": [')
        idx4 = self.html_text[idx0:].find('], "G": [')
        idx5 = self.html_text[idx0:].find('], "N": [')
        idx6 = self.html_text[idx0:].find(']};')

        # output
        distA = self.html_text[(idx0 + idx1 + len('{"A": [')):idx0 + idx2]
        distAa = np.array([float(x) for x in distA.split(", ")])

        distC = self.html_text[(idx0 + idx2 + len('], "C": [')):idx0 + idx3]
        distCa = np.array([float(x) for x in distC.split(", ")])

        distT = self.html_text[(idx0 + idx3 + len('], "T": [')):idx0 + idx4]
        distTa = np.array([float(x) for x in distT.split(", ")])

        distG = self.html_text[(idx0 + idx4 + len('], "G": [')):idx0 + idx5]
        distGa = np.array([float(x) for x in distG.split(", ")])

        distN = self.html_text[(idx0 + idx5 + len('], "N": [')):idx0 + idx6]
        distNa = np.array([float(x) for x in distN.split(", ")])
        self.basetype_dist = {"A": distAa, "C": distCa, "T": distTa, "G": distGa, "N": distNa}

    def extract_discordance(self):
        ### find 'var varRateDist'
        idx0 = self.html_text.find('var varRateDist')
        idx1 = self.html_text[idx0:].find('Mismatch')
        idx2 = self.html_text[idx0:].find('MismatchNoN')
        idx3 = self.html_text[idx0:].find('Gap\\\':')
        idx4 = self.html_text[idx0:].find(']};')

        # print( idx1,idx2,idx3,idx4)
        # output
        mismatch = self.html_text[(idx0 + idx1 + len('Mismatch') + 5):idx0 + idx2 - 5]
        try:
            mismatch = np.array([float(x) for x in mismatch.split(", ")])
        except Exception as e:
            mismatch = None

        mismatchNoN = self.html_text[(idx0 + idx2 + len('MismatchNoN') + 5):idx0 + idx3 - 5]
        try:
            mismatchNoN = np.array([float(x) for x in mismatchNoN.split(", ")])
        except Exception as e:
            mismatchNoN = None

        gap = self.html_text[(idx0 + idx3 + len("Gap\\\':")) + 2:idx0 + idx4]
        try:
            gap = np.array([float(x) for x in gap.split(", ")])
        except Exception as e:
            gap = None
        self.discordance = {"Mismatch":mismatch, "MismatchNoN":mismatchNoN, "Gap":gap}

    def extract_SummaryTable(self):
        # extract information from Summary Table and convert to dict
        idx0 = self.html_text.find('var summaryTable = [')
        idx1 = self.html_text[idx0:].find('];')
        summaryTable = self.html_text[idx0 + len("var summaryTable = ["):idx0 + idx1]
        summaryTable = summaryTable.replace("\\", "")
        summaryTable = summaryTable.replace("[", "").replace("]", "").replace("'", "")
        summaryList = summaryTable.split(",")

        summaryDict = List2Dic(summaryList)

        if 'FqReads(M)' not in summaryDict.keys():  #manually calc and add FqReads(M)
            try:
                active_pix_m = float(summaryDict['ActivePixelsNum(M)'])
                loading_rate = float(summaryDict['LoadingRate%'])/100
                fq_reads = str(round(active_pix_m*loading_rate, 2))
                summaryDict['FqReads(M)'] = fq_reads
            except Exception as e:
                pass
        self.summaryTable = summaryDict

    def save_plot(self, data_dict, plotstr, y_label, save_path, marks, lines, colors):


        fig, ax = plt.subplots(figsize = (4.9,5))


        for i, line_lab_key in enumerate(data_dict.keys()):
            line_vals = data_dict[line_lab_key]
            if line_vals is None:
                max_vals = 11
                line_vals = np.zeros(10)
            else:
                max_vals = len(line_vals)+1
            ax.plot(np.arange(1, max_vals), line_vals, label=line_lab_key,color = colors[i], linestyle=lines[i],
                    marker=marks[i], linewidth=1, markersize=3)

        ymin, ymax = plt.ylim()
        plt.xlabel("Cycle Number", fontsize = 20)
        #plt.xticks(np.arange(0, max_vals, ))
        plt.title(plotstr)

        plt.ylabel(y_label)
        plt.legend(fontsize=12)
        output_path = os.path.join(save_path, plotstr+".png")
        plt.tight_layout()
        fig.savefig(output_path)
        plt.close()
        return output_path

    def output_plots(self, save_dir):

        index = ['Img Dark', 'Img Signal', 'ImgBioBk', 'Im SNR', 'Seq SNR', 'BIC', 'Q30', 'Lag_Runon',
                 'Discordance', 'Mapped Base Distribution']
        lag_run_marks = ["o", "+", "^", "o", "+", "^"]
        lag_run_lines = ['solid', 'dashed', 'dotted', 'solid', 'dashed', 'dotted']
        lag_run_colors = ['green', 'green', 'grey', 'red', 'red', 'grey']

        self.temp_plot_paths[index[0]] = self.save_plot(self.darkimage_results, index[0],
                                                        "Image Dark (DN)", save_dir, marks=["o", "o"],
                                                        lines=['solid', 'solid'], colors=['red', 'green'])
        self.temp_plot_paths[index[1]] = self.save_plot(self.imgsignal, index[1],
                                                        "Image Signal (DN)", save_dir, marks=["o", "o"],
                                                        lines=['solid', 'solid'], colors=['red', 'green'])
        self.temp_plot_paths[index[2]] = self.save_plot(self.im_biobk, index[2],
                                                        "Image Bio Bk (DN)", save_dir, marks=["o", "o"],
                                                        lines=['solid', 'solid'], colors=['red', 'green'])
        self.temp_plot_paths[index[3]] = self.save_plot(self.im_snr, index[3],
                                                        "Image SNR", save_dir, marks=["o", "o"],
                                                        lines=['solid', 'solid'], colors=['red', 'green'])
        self.temp_plot_paths[index[4]] = self.save_plot(self.seqsnr, index[4],
                                                        "Seq SNR", save_dir, marks=["o", "o"],
                                                        lines=['solid', 'solid'], colors=['red', 'green'])
        self.temp_plot_paths[index[5]] = self.save_plot(self.bic, index[5],
                                                        "Percentage (%)", save_dir, marks=["o"],
                                                        lines=['solid'], colors=['red'])
        self.temp_plot_paths[index[6]] = self.save_plot(self.q30, index[6],
                                                        "Percentage (%)", save_dir, marks=["o"],
                                                        lines=['solid'], colors=['red'])
        comb_lag_runnon = {**self.runon, **self.lag}
        self.temp_plot_paths[index[7]] = self.save_plot(comb_lag_runnon, index[7],
                                                        "Lag and Runon (%)", save_dir, marks=lag_run_marks,
                                                        lines=lag_run_lines, colors=lag_run_colors)
        self.temp_plot_paths[index[8]] = self.save_plot(self.discordance, index[8],
                                                        "Percentage (%)", save_dir, marks=['o', 'o', 'o'],
                                                        lines=['solid', 'solid', 'solid'], colors=['red', 'green', 'gold'])
        self.temp_plot_paths[index[9]] = self.save_plot(self.basetype_dist, index[9],
                                                        "Percentage (%)", save_dir, marks=['o', 'o', 'o', 'o', 'o'],
                                                        lines=['solid', 'solid', 'solid', 'solid', 'solid'],
                                                        colors=['red', 'green', 'gold', 'blue', 'grey'])

        print(self.temp_plot_paths)

    def make_html_dfrow(self, se_cycle1, pe_cycle1):
        se_cycle1_idx = se_cycle1-1
        if pe_cycle1 is None:
            pe_cycle1_idx = 0
        else:
            pe_cycle1_idx = se_cycle1 + pe_cycle1-1

        column_dicts = [self.imgsignal, self.im_biobk, self.seqsnr, self.im_snr, self.bic, self.q30]

        column_names = ['Image signal', 'ImgBioBk', 'SeqSNR', 'ImgSNR', 'BIC', 'Q30'] #BIC, Q30 not Img1, Img2

        se_row_dict, pe_row_dict = {}, {}

        for i, col_name, col_dict in zip(range(len(column_names)), column_names, column_dicts):
            if i < 4:
                for img in ['Image1', 'Image2']:
                    se_row_dict['SE '+column_names[i]+' '+img] = col_dict[img][se_cycle1_idx]
                    pe_row_dict['PE '+column_names[i]+' '+img] = col_dict[img][pe_cycle1_idx]
            else:
                se_row_dict['SE '+ column_names[i]] = col_dict[column_names[i]][se_cycle1_idx]
                pe_row_dict['PE '+ column_names[i]] = col_dict[column_names[i]][pe_cycle1_idx]

        se_cyc1_df = pd.DataFrame(se_row_dict, index=[0])
        pe_cycle1_df = pd.DataFrame(pe_row_dict, index=[0])
        #print(se_cyc1_df, pe_cycle1_df, sep="\n")

        #print(self.summaryTable)
        summaryTable_df = pd.DataFrame(self.summaryTable, index = [0])
        summaryTable_df = summaryTable_df.rename({'BaseCallVer':"BaseCallVersion", 'TemplateVer':"TemplateVersion"}, axis=1)
        keep_cols = ["Reference", "CycleNumber", "ActivePixelsNum(M)", "MappedReads(M)", "LoadingRate%", "Q30%",
                     "Lag1%", "Lag2%", "Runon1%", "Runon2%", "MappingRate%", "AvgErrorRate%", "AvgErrorRate!N%",
                     "MappingRate%-R1", "MappingRate%-R2", "Error Rate%-R1", "Error Rate%-R2", 'Throughput(GB)', "NSB%"]
        unique_cols = ["BaseCallVer","BaseCallVersion", "TemplateVer", "TemplateVersion", "Reference", "CycleNumber", "ActivePixelsNum", "TotalReads",
                       "FqReads", "MappedReads", "AvgDuplicationRate", "LoadingRate", "ESR", "Q30",
                     "Lag", "Runon", "MappingRate", "AvgErrorRate", "MappingRate", "Error Rate", "Throughput", "NSB"]
        pattern = "|".join(unique_cols)
        if pe_cycle1 is None:
            empty_pe_df = pd.DataFrame(columns=pe_cycle1_df.columns)
            pe_cycle1_df = empty_pe_df

        summary_keep_df = summaryTable_df.loc[:, summaryTable_df.columns.str.contains(pattern)]
        meta_df_addrow = pd.concat([se_cyc1_df, pe_cycle1_df, summary_keep_df], axis=1)
        #print(meta_df_addrow)
        return meta_df_addrow


class rawImage_Folder:

    def __init__(self, rawfolder_path, scale_dir):
        self.outer_path = rawfolder_path
        self.cycle_folders = next(os.walk(os.path.join(self.outer_path)))[1]  #list of directories in raw folder
        print(self.cycle_folders)
        self.cycle_list = [int(c_name.split("_")[0][1:]) for c_name in self.cycle_folders]
        path_dict = {}
        img_idx = 0
        for cyc_num, folder in zip(self.cycle_list, self.cycle_folders):
            files = os.listdir(os.path.join(self.outer_path, folder))
            img_list = ["%s_"%str(cyc_num)+"_".join(image.split(".")[0].split("_")[:2]) for image in files]
            path_list = [os.path.join(self.outer_path, folder, file) for file in files]
            for path, img_name in zip(path_list, img_list):
                path_dict[img_idx] = [cyc_num, img_name, path]
                img_idx += 1
        self.raw_img_df = pd.DataFrame.from_dict(path_dict, orient='index', columns=['Cycle Number', 'Image Name',
                                                                                     'Image Path'])
        self.output_scale_dir = scale_dir


    def output_images(self):

        output_path_dict = {}
        #print(self.raw_img_df)
        out_path_newcol = []
        for row_idx in self.raw_img_df.index.values:
            row = self.raw_img_df.iloc[row_idx, :]
            #print(row)
            img_out = SensorImage(row['Image Path'], row['Image Name'], self.output_scale_dir)
            out_path_newcol.append(img_out.save_dir)
        self.raw_img_df['Scale Path'] = out_path_newcol
        return self.raw_img_df

    def first_last_sharepoint_link(self):

        signal_img = self.raw_img_df.loc[~self.raw_img_df['Image Name'].str.contains('Dark'), :]
        min_cycle = signal_img.loc[signal_img['Cycle Number'] == np.amin(signal_img['Cycle Number'].values), :]
        max_cycle = signal_img.loc[signal_img['Cycle Number'] == np.amax(signal_img['Cycle Number'].values), :]

        first = min_cycle['Scale Path'].values[0]
        last = max_cycle['Scale Path'].values[0]
        #print(first, last)

        trim_path = 'ScaledImages'

        out_path_html = []
        for item in [first, last]:
            if item is None:
                out_path_html.append(None)
            else:
                raw_str = trim_path + "/" + item
                fmt_slash = raw_str.replace("/", "%2F")
                fmt_underscore = fmt_slash.replace("_", "%5F")
                fmt_period = fmt_underscore.replace(".", "%2E")
                out_path_html.append(fmt_period)


        return out_path_html, trim_path#[x[x.find('\\'+trim_path):] for x in [first, last]]


class SensorImage:

    rows = 3072
    cols = 4096
    image_path = None
    scalefactor = 5
    out_name = None
    image_arr = None

    def __init__(self, folderpath, name, scaled_image_dir):
        super(SensorImage, self).__init__()
        self.read_image(folderpath)
        self.out_name = name
        self.scale_dir = scaled_image_dir
        self.save_image()


    def read_image(self, folder):
        self.image_path = folder
        #print(folder)
        with open(self.image_path, 'rb') as fd:
            img = np.fromfile(fd, dtype=np.uint16, count=self.rows * self.cols)
            img = img.reshape((self.rows, self.cols))
            img = img.astype(np.float64)
            #img = img[::self.scalefactor, ::self.scalefactor]  #normalize scales?
            self.image_arr = img
            fd.close()


    def save_image(self):

        dconv = self.image_arr.astype(np.uint16)
        img_scaled = normalize(dconv, dst=None, alpha=15, beta=1600, norm_type=NORM_MINMAX)
        outfilename = self.out_name + ".jpg"
        self.save_dir = os.path.join(self.scale_dir, outfilename)
        #print(self.save_dir)
        save_change_slash = self.save_dir.replace("\\", "/")
        print(save_change_slash)
        ret = imwrite(save_change_slash, img_scaled)
        print(outfilename, ret)


class scatter_Folder:

    def __init__(self, scatter_folderPath):
        self.outer_path = scatter_folderPath
        self.inner_folder = next(os.walk(self.outer_path))[1]
        if len(self.inner_folder) > 1:
            print("Multiple Scatter Folders Found")
        elif len(self.inner_folder) == 0:
            self.scatter_images = os.listdir(self.outer_path)
            self.register_cycles()
        else:
            self.scatter_images = os.listdir(os.path.join(self.outer_path, self.inner_folder[0]))
            self.register_cycles()
            print(self.scatter_register)

    def register_cycles(self):
        register_dict = {}
        for file in self.scatter_images:
            cycle = int(file.split("_")[-1][:-4])
            register_dict[cycle] = file
        self.scatter_register = register_dict

    def first_last_scatter(self, first=None, last=None):
        scatter_cycles = list(self.scatter_register.keys())
        print(scatter_cycles)
        if first is None:
            first_scatter = scatter_cycles[np.argmin(scatter_cycles)]
            print(first_scatter)
        else:
            first_scatter = scatter_cycles[first+1]
        if last is None:
            last_scatter = scatter_cycles[np.argmax(scatter_cycles)]
            print(last_scatter)
        else:
            last_scatter = scatter_cycles[last+1]
        if len(self.inner_folder) == 0:
            return [os.path.join(self.outer_path, x) for x in [self.scatter_register[first_scatter],
                                                                                 self.scatter_register[last_scatter]]]
        else:
            return [os.path.join(self.outer_path, self.inner_folder[0], x) for x in [self.scatter_register[first_scatter],
                                                                                 self.scatter_register[last_scatter]]]

    def first_last_sharepointpath(self):
        first, last = self.first_last_scatter()
        trim_path = self.outer_path.split("\\")[-1]
        #  "%2F1%5FDark%5FImg1%2Ejpg"
        #  "1_Dark_Image1.jpg"
        out_path_html = []
        for item in [first, last]:
            if item is None:
                out_path_html.append(None)
            else:
                raw_str = trim_path+"/"+item
                fmt_slash = raw_str.replace("/", "%2F")
                fmt_underscore = fmt_slash.replace("_", "%5F")
                fmt_period = fmt_underscore.replace(".", "%2E")
                out_path_html.append(fmt_period)
        return out_path_html, trim_path

class operationlog_Folder:

    def __init__(self, operation_log_path):
        self.folder_path = operation_log_path
        self.register_files()
        print(self.pressure_log, self.temperature_log, self.ops_logs, sep="\n")

    def register_files(self):
        files = os.listdir(self.folder_path)
        pressure = []
        temperature = []
        logs = []
        for f in files:
            if f.endswith(".txt"):
                pressure.append(f)
            elif f.endswith(".csv"):
                temperature.append(f)
            elif "log" in f:
                logs.append(f)
            else:
                print("Uknown file in Operation Log Folder, %s"%f)
        self.pressure_log = pressure
        self.temperature_log = temperature
        self.ops_logs = logs

class meta_output:

    def __init__(self, new_dfrow, meta_df_path, new_ver_dfrow):
        self.new_add_dfrow = new_dfrow
        self.meta_tracker_path = meta_df_path
        self.new_ver_dfrow = new_ver_dfrow
        self.import_concat_tracker()
        #add import and concat dfs
        self.output_df()

    def import_concat_tracker(self):
        #sort for added columns, reorder after concat
        old_tracker = pd.read_excel(self.meta_tracker_path, sheet_name='Master Tracker', index_col=[0], skiprows=[0])
        old_cols = old_tracker.columns.values
        new_cols = self.new_add_dfrow.columns.values
        final_newcols = []
        most_cols = np.argmax([len(new_cols), len(old_cols)])
        min_cols = 1 if most_cols == 0 else 0
        col_list = [new_cols, old_cols]
        main = col_list[most_cols]
        other = col_list[min_cols]
        try:
            while(len(main) > 0):
                m_col, main = main[0], main[1:]
                n_col, other = other[0], other[1:]

                if m_col == n_col:
                    final_newcols.append(m_col)
                elif n_col in main:
                    final_newcols.append(m_col)
                    other = np.insert(other, 0, n_col)
                elif m_col in other:
                    final_newcols.append(n_col)
                    main = np.insert(main, 0, m_col)
                else:
                    final_newcols.append(m_col)
                    final_newcols.append(n_col)

        except IndexError:
            if len(main) > 0:
                final_newcols.extend(main)
            elif len(other) > 0:
                final_newcols.extend(other)

        new_df = pd.concat([old_tracker, self.new_add_dfrow], axis=0, ignore_index=True)
        ordered_cols = new_df.reindex(columns=final_newcols)
        self.new_tracker = ordered_cols

        old_version = pd.read_excel(self.meta_tracker_path, sheet_name='TrackerVersion', index_col=[0])
        new_versiondf = pd.concat([old_version, self.new_ver_dfrow], axis=0, ignore_index=True)

        self.new_version = new_versiondf

    def output_df(self):

        self.writer = pd.ExcelWriter(self.meta_tracker_path, engine='xlsxwriter')

        self.new_tracker.to_excel(self.writer, sheet_name='Master Tracker', startrow=2, header=False)
        #insert formatting fcn
        self.add_fmt_groups()
        self.add_fmt_fold()
        self.add_fmt_header()
        self.new_version.to_excel(self.writer, sheet_name='TrackerVersion')
        #insert formatting fcn
        self.writer.save()

    def add_fmt_fold(self):
        sheet = self.writer.sheets['Master Tracker']

        fold_points = [["DNB sample ID", "Other comment ", "ISC ID"],
                       ["ISC Mfg Date", "ISC 备注", "IRC ID"],
                       ["IRC Mfg Date", "IRC 备注", "Instrument ID"],
                       ["Event ID", "Notes", "Cycle 1 Image"],
                       ["NonspecificAdsorptionPct(%)\nTestArea_1", "NonspecificAdsorptionPct(%)\nTestArea_31", "NSB(%)"],
                       ["SE ImgBioBk Image1", "SE BIC", "SE Q30"],
                       ["PE ImgBioBk Image1", "PE BIC", "PE Q30"]
                       ]
        cols = self.new_tracker.columns.values
        fold_points_index = []
        for col0, col1, col2 in fold_points:
            idx0 = None
            idx1 = None
            idx2 = None
            for i in range(len(cols)):
                col_idx = i+1
                cur_col = cols[i]
                if col0 == cur_col:
                    idx0 = col_idx
                elif col1 == cur_col:
                    idx1 = col_idx
                elif col2 == cur_col:
                    idx2 = col_idx
                else:
                    continue
            if idx0 is not None:
                if idx1 is not None:
                    if idx2 is not None:
                        fold_points_index.append([idx0, idx1, idx2])

        for ittr, idx_lst in enumerate(fold_points_index):
                col_start = xl_col_to_name(idx_lst[0])
                col_end = xl_col_to_name(idx_lst[1])
                collapsed = xl_col_to_name(idx_lst[2])
                sheet.set_column("%s:%s"%(col_start, col_end), 13, None, {'level': 1, 'hidden':True})
                sheet.set_column("%s:%s"%(collapsed, collapsed), 13, None, {'collapsed': True})

    def add_fmt_header(self):
        cols = self.new_tracker.columns.values
        wb = self.writer.book
        worksheet = self.writer.sheets['Master Tracker']

        larger_cols = ['Sharepoint Directory', 'Reference',	'CycleNumber', "ActivePixelsNum(M)",
                       "HTML Input MappedReads(M)", "LoadingRate%", "Q30%", "HTML Input Lag%", "HTML Input Runon%",
                       "MappingRate%", "AvgErrorRate%", "AvgErrorRate!N%"]

        for col_num, value in enumerate(cols):
            if value in larger_cols:
                worksheet.set_column(col_num+1, col_num+1, 15)

        header_format = wb.add_format({
            'bold': True,
            'text_wrap': True,
            'valign': 'top',
            'border': 1
        })
        for col_num, value in enumerate(cols):
            worksheet.write(1, col_num+1, value, header_format)

    def add_fmt_groups(self):

        ws = self.writer.sheets['Master Tracker']

        fmt_dict = {
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'gray'
        }

        groups = {'UI Input': '#E2EFDA', 'Experiment Input': '#DDEBF7', 'Image': "#FCE4D6", 'Scatter': "#F8C1A2",
                  'Testing Area Matrix': "#D9E1F2", 'Sequencing of SE1': "FFE699", 'Sequencing of PE1': "FFDE75",
                  'Whole Sequencing Result': "C6E0B4"}

        index_endpoints = {
            "UI Input": ["Test Date", "Sharepoint Directory"],
            "Experiment Input":["DNB ID", "Notes"],
            "Image": ["Cycle 1 Image", "Last Cycle Image"],
            "Scatter": ["Cycle 1 Scatter", "Last Cycle Scatter"],
            "Testing Area Matrix": ["NonspecificAdsorptionPct(%)\nTestArea_1", "Test Area Filtered Out"],
            "Sequencing of SE1": ["SE Image signal Image1", "SE Q30"],
            'Sequencing of PE1': ["PE Image signal Image1", "PE Q30"],
            "Whole Sequencing Result": ["BaseCallVersion", "AvgErrorRate!N%"]
        }
        endpoints_column_number = {}
        for group_name in groups.keys():
            cols = self.new_tracker.columns.values
            end_idx0 = None
            end_idx1 = None
            for col_num, name in enumerate(cols):
                if index_endpoints[group_name][0] == name:
                    end_idx0 = col_num
                elif index_endpoints[group_name][1] == name:
                    end_idx1 = col_num
                    break
            if end_idx0 is None:
                continue
            elif end_idx1 is None:
                continue
            endpoints_column_number[group_name] = [end_idx0, end_idx1]
            new_format_dict = fmt_dict
            new_format_dict['fg_color'] = groups[group_name]
            new_format = self.writer.book.add_format(new_format_dict)
            range = xl_range(0, end_idx0+1, 0, end_idx1+1)

            ws.merge_range(range, group_name, new_format)







def zip_folder(folder_path, output_path):
    """Zip the contents of an entire folder (with that folder included
    in the archive). Empty subfolders will be included in the archive
    as well.
    """
    parent_folder = os.path.dirname(folder_path)
    # Retrieve the paths of the folder contents.
    contents = os.walk(folder_path)
    try:
        zip_file = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)
        ZipDir(folder_path, zip_file, ignoreDir=["Raw"], ignoreExt=['.raw'])
    except IOError as message:
        print(message)
        return False
    except OSError as message:
        print(message)
        return False
    except zipfile.BadZipfile as message:
        print(message)
        return False
    finally:
        zip_file.close()
        return True


def IsPathValid(path, ignoreDir, ignoreExt):
    splited = None
    if os.path.isfile(path):
        if ignoreExt:
            _, ext = os.path.splitext(path)
            if ext in ignoreExt:
                return False

        splited = os.path.dirname(path).split('\\/')
    else:
        if not ignoreDir:
            return True
        splited = path.split('\\/')

    for s in splited:
        if s in ignoreDir:  # You can also use set.intersection or [x for],
            return False

    return True

def zipDirHelper(path, rootDir, zf, ignoreDir = [], ignoreExt = []):
    # zf is zipfile handle
    if os.path.isfile(path):
        if IsPathValid(path, ignoreDir, ignoreExt):
            relative = os.path.relpath(path, rootDir)
            zf.write(path, relative)
        return

    ls = os.listdir(path)
    for subFileOrDir in ls:
        if not IsPathValid(subFileOrDir, ignoreDir, ignoreExt):
            continue

        joinedPath = os.path.join(path, subFileOrDir)
        zipDirHelper(joinedPath, rootDir, zf, ignoreDir, ignoreExt)

def ZipDir(path, zf, ignoreDir = [], ignoreExt = []):
    rootDir = path if os.path.isdir(path) else os.path.dirname(path)
    zipDirHelper(path, rootDir, zf, ignoreDir, ignoreExt)
    pass

def zipIgnoreDir(path, rootDir, zf):
    # recursivly zips previously ignored dir, on size reqs

    if os.path.isfile(path):
        relative = os.path.relpath(path, rootDir)
        zf.write(path, relative)
        return

    ls = os.listdir(path)
    for subFileOrDir in ls:
        joinedPath = os.path.join(path, subFileOrDir)
        zipIgnoreDir(joinedPath, rootDir, zf)






def List2Dic(summaryList):
    result = {}
    n = len(summaryList)
    for n in range(2, n - 1, 2):
        key_drop = summaryList[n].replace(" ", "")
        value_drop = summaryList[n+1].replace(" ", "")
        result[key_drop] = value_drop
    return result


if __name__ == '__main__':
    cwd = os.getcwd()
    print(cwd)
    dirs = next(os.walk(cwd))[1]
    print(dirs)
    select_dir = dirs[11]
    start_path = os.path.join(cwd, select_dir)
    save_path = os.path.join(cwd, 'Save_folder')
    run = RunFolder(start_path, save_path)
    run.init_extract()
    run.html_file.output_plots(save_path)
    row = run.html_file.make_html_dfrow(1, 0)